{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}

-- This is the main configuration file for Propellor, and is used to build
-- the propellor program.

-- import Control.Monad.Extra (ifM)
import           Control.Applicative                    ((<$>))
import           Control.Monad.IO.Class                 (liftIO)
import           Data.List                              hiding (partition)
import           System.FilePath                        (takeDirectory, (</>))

import           Propellor
import           Propellor.PrivData
import           Propellor.Property.Apt                 (Package, Url)
import           Propellor.Property.Bootstrap
import           Propellor.Property.Gpg                 (GpgKeyId (..))
import           Propellor.Property.Scheduled
import           Propellor.Property.Versioned

import qualified Propellor.Property.Apt                 as Apt
import qualified Propellor.Property.ConfFile            as ConfFile
import qualified Propellor.Property.Cron                as Cron
import qualified Propellor.Property.Docker              as Docker
import qualified Propellor.Property.File                as File
import qualified Propellor.Property.FreeDesktop         as FreeDesktop
import qualified Propellor.Property.Fstab               as Fstab
import qualified Propellor.Property.Git                 as Git
import qualified Propellor.Property.Group               as Group
import qualified Propellor.Property.Grub                as Grub
import qualified Propellor.Property.Hostname            as Hostname
import qualified Propellor.Property.Mount               as Mount
import qualified Propellor.Property.Network             as Network
import qualified Propellor.Property.PropellorRepo       as PropellorRepo
import qualified Propellor.Property.Sbuild              as Sbuild
import qualified Propellor.Property.Schroot             as Schroot
import qualified Propellor.Property.Service             as Service
import qualified Propellor.Property.SiteSpecific.Soleil as Soleil
import qualified Propellor.Property.Ssh                 as Ssh
import qualified Propellor.Property.Sudo                as Sudo
import qualified Propellor.Property.Systemd             as Systemd
import qualified Propellor.Property.Timezone            as Timezone
import qualified Propellor.Property.User                as User

main :: IO ()
main = defaultMain hosts

-- | The hosts propellor knows about.
hosts :: [Host]
hosts =
      [ mordor
      , pupuce
      , ssd
--       , ord03244
      -- Soleil RS
      , jupyter
      , ord03037
      -- Soleil RS-serveur
      , grades_01 -- 195.221.4.1
      -- Soleil REL
      , cristal4 -- 195.221.8.228
      , diffabs6 -- 195.221.8.166
      , hermes17 -- 195.221.13.113
      , mars2 -- 195.221.7.2
      , nanoscopium8 -- 195.221.13.40
      , re_grades_01 -- 195.221.10.46
      -- , nanoscopium11 -- 195.221.13.43
      , ode15
      , proxima1_23
      , proxima1_26
      , rock1 -- 195.221.13.161
      , rock2  -- 195.221.13.162
      , sextants6 -- 195.221.8.134
      , sixs3 -- 195.221.9.195
      , sixs7 -- 195.221.9.199
      -- Soleil RCL
      , collect1
      -- serge
      , cush
      ]

-- | packages

packagesToRemove :: [Package]
packagesToRemove =
  [ "docker.io"
  , "python3-pip"
  , "python-pip"
  ]

packages :: [Package] -> Property Debian
packages extra = do
  let common = ["firmware-linux-nonfree"
               , "arandr"
               , "apt-transport-https"
               , "apt-file"
               , "autoconf-archive"
               , "autopkgtest"
               , "black"
               , "blends-dev"
               , "blhc"
               , "bonnie++"
               , "cabal-debian"
               , "cabal-install"
               , "ca-certificates"
               , "chrpath"
               , "cme"
               -- , "clang" -- problem with unstable for now
               , "collectd"
               , "cppcheck"
               , "cups"
               , "cups-bsd"
               , "dctrl-tools"
               , "debconf-utils"
               , "debhelper"
               , "debian-goodies"
               , "devscripts"
               , "dput-ng"
               , "dpkg-dev-el"
               , "dnsutils"
               , "dgit"
               , "dh-make"
               , "etckeeper"
	       , "emacs"
               , "elpa-company"
               , "elpa-flycheck"
               , "elpa-powerline"
               , "ess"
               , "exfat-utils"
               , "fabio-viewer"
               , "flake8"
               , "gdb"
               , "gdisk"
               , "ghc-doc"
               , "ghc-prof"
               , "ghkl"
	       , "git"
	       , "gitk"
               , "git-annex"
               , "git-buildpackage"
               , "git-dpm"
               , "git-email"
	       , "git-gui"
               , "glances"
               , "gobby"
               , "gparted"
               , "gsmartcontrol"
	       , "haskell-mode"
               , "haskell-devscripts"
               , "haskell-platform"
               , "hdf5-helpers"
               , "hdf5-tools"
               , "help2man"
               , "hfsplus"
               , "hfsutils"
               , "hfsprogs"
               , "hfsutils-tcltk"
               , "hlint"
               , "hpack"
               , "htop"
               , "hwloc"
               , "imagej"
               , "inkscape"
               , "iozone3"
               , "jupyter-notebook"
               , "jq"
               , "kcachegrind"
               , "kcachegrind-converters"
               , "kcollectd"
               , "libdpkg-perl"
               , "libfile-slurp-perl"
               , "libghc-gtk3-prof"
               , "libghc-hmatrix-gsl-prof"
               , "libhdf5-dev"
               , "libhkl-dev"
               , "libipc-run-perl"
               , "liblist-moreutils-perl"
               , "libtext-patch-perl"
               , "libreoffice"
               , "lintian"
               , "liburi-perl"
               , "lldb"
               , "magit"
               , "mayavi2"
               , "mc"
               , "mdadm"
               , "nfs-common"
               , "nitrokey-app"
	       , "moreutils"
               , "npm"
               , "npm2deb"
	       , "mr"
	       , "mutt"
               , "mypy"
               , "numactl"
               , "numatop"
               , "octave"
               , "octave-image"
               , "octave-netcdf"
               , "paraview"
               , "patchelf"
               , "patchutils"
               , "pigz"
               , "piuparts"
               , "pkg-haskell-tools"
               , "pyfai"
               , "pymca"
               , "python-matplotlib-doc"
               , "python3-guiqwt"
               , "python3-lmfit"
               , "python3-matplotlib"
               , "python3-mpltoolkits.basemap"
               , "python3-numpy"
               , "python3-opencv"
               , "python3-pyopencl"
               , "python3-pyinotify"
               , "python3-pyperclip"
               , "python3-scipy"
               , "python3-sklearn"
               , "python3-statsmodels"
               , "python3-stdeb"
               , "python3-tables"
               , "qtikz"
               , "r-base"
               , "r-cran-rcpp"
               , "rsync"
               , "sbuild"
               , "scdaemon"
               , "silx"
               , "spyder3"
	       , "ssh"
               , "ssh-askpass"
               , "stylish-haskell"
               , "texmaker"
               , "texlive-latex-extra"
               , "texstudio"
               , "tree"
               , "ubuntu-dev-tools"
               , "udisks2"
               , "xclip"
               , "xfsprogs"
               , "whois"
               ]
  let stretch = [ "hothasktags"
                , "hdfview"
                , "pypi2deb"
                , "python-guiqwt"
                , "python-lmfit"
                , "python-matplotlib"
                , "python-mpltoolkits.basemap"
                , "python-numpy"
                , "python-opencv"
                , "python-pyopencl"
                , "python-pyinotify"
                , "python-scipy"
                , "python-stdeb"
                , "python-tables"
                , "spyder"
                , "xul-ext-adblock-plus"
                , "xul-ext-noscript"
                ] ++ common
  let buster = [ "hothasktags"
               , "lintian-brush"
               , "ufo-filters"
               , "webext-debianbuttons"
               , "webext-lightbeam"
               , "webext-https-everywhere"
               , "webext-noscript"
               , "webext-privacy-badger"
               , "webext-ublock-origin"
               ] ++ common
  let bullseye = [ "lintian-brush"
                 , "openssh-client-ssh1"
                 , "python-xrayutilities-doc"
                 , "python3-xrayutilities"
                 , "ufo-filters"
                 , "webext-debianbuttons"
                 , "webext-lightbeam"
                 , "webext-https-everywhere"
                 , "webext-privacy-badger"
                 , "webext-ublock-origin"
                 ] ++ common
  let unstable = bullseye ++ common

  withOS ("Install common packages") $ \w o -> ensureProperty w $
      case o of
        (Just (System (Debian _ (Stable "bullseye")) _))  -> Apt.installed (bullseye ++ extra)
        (Just (System (Debian _ (Stable "buster")) _))  -> Apt.installed (buster ++ extra)
        (Just (System (Debian _ (Stable "stretch")) _)) -> Apt.installed (stretch ++ extra)
        (Just (System (Debian _ Unstable) _))           -> Apt.installed (unstable ++ extra)
        _                                               -> error $ " backports installation not yet implemented on " ++ show o

backports :: [Package] -> Property Debian
backports extra = do
  let common = [ "dgit"
               , "debhelper"
               , "devscripts"
               , "fabio-viewer"
               , "git-debrebase"
               , "pyfai"
               , "python-fabio-doc"
               , "python-pyfai-doc"
               , "python3-fabio"
               , "python3-pyfai"
               , "python3-silx"
               , "silx"
               ]
  let stretch = [ "python-fabio"
                , "python-pyfai"
                , "python-silx"
                , "sbuild"
                ] ++ common
  let buster = common
  withOS ("Install backports") $ \w o -> ensureProperty w $
    case o of
      (Just (System (Debian _ (Stable "bullseye")) _))  -> doNothing
      (Just (System (Debian _ (Stable "buster")) _))  -> Apt.backportInstalled (buster ++ extra)
      (Just (System (Debian _ (Stable "stretch")) _)) -> Apt.backportInstalled (stretch ++ extra)
      (Just (System (Debian _ Unstable) _))           -> doNothing
      _                                               -> error $ "backports installation not implemented on " ++ show o

-- | window manager

data WindowManager = Gnome | Awesome | Xfce4 | Xmonad

xorg :: [Package]
xorg = ["xorg"]

wm :: WindowManager -> [Package]
wm Gnome = xorg ++ ["gnome"]
wm Awesome = xorg ++ ["awesome"]
wm Xfce4 = xorg ++ ["xfce4"]
wm Xmonad = xorg ++ [ "xmonad"
                    , "suckless-tools"]

-- | Sbuild setup

sbuild :: Architecture -> User -> Host -> Property (HasInfo + DebianLike)
sbuild arch user h = propertyList "setup sbuild" $ props
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian Unstable arch
      & Sbuild.update `period` Daily
      & Sbuild.useHostProxy h)
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian (Stable "buster") arch
      & Sbuild.update `period` Weekly (Just 1)
      & Sbuild.useHostProxy h)
  ! (Sbuild.built Sbuild.UseCcache $ props
      & osDebian (Stable "stretch") arch
      & Sbuild.update `period` Weekly (Just 1)
      & Sbuild.useHostProxy h)
  & Sbuild.userConfig user
  & Sbuild.usableBy user

-- | redirect all root email to an email

data EximType = Satellite | Smarthost

instance Show EximType where
  show Satellite = "satellite"
  show Smarthost = "smarthost"

redirectRoot :: String -> String -> EximType -> Property DebianLike
redirectRoot email smarthost eximtype = propertyList ("redirect root email to " ++ email) $ props
                                        & redirect
                                        & configure smarthost eximtype
                                        & installed
  where
    redirect :: Property UnixLike
    redirect = "/etc/exim4/conf.d/rewrite/00_exim4-config_header"
               `File.hasContent`
               [ "######################################################################"
               , "#                      REWRITE CONFIGURATION                         #"
               , "######################################################################"
               , ""
               , "begin rewrite"
               , ""
               , "root@* " ++ email ++ " FfrsTtcb"
               ]

    configure :: String -> EximType -> Property UnixLike
    configure s t = "/etc/exim4/update-exim4.conf.conf"
                  `File.hasContent`
                  [ "# /etc/exim4/update-exim4.conf.conf"
                  , "#"
                  , "# Edit this file and /etc/mailname by hand and execute update-exim4.conf"
                  , "# yourself or use 'dpkg-reconfigure exim4-config'"
                  , "#"
                  , "# Please note that this is _not_ a dpkg-conffile and that automatic changes"
                  , "# to this file might happen. The code handling this will honor your local"
                  , "# changes, so this is usually fine, but will break local schemes that mess"
                  , "# around with multiple versions of the file."
                  , "#"
                  , "# update-exim4.conf uses this file to determine variable values to generate"
                  , "# exim configuration macros for the configuration file."
                  , "#"
                  , "# Most settings found in here do have corresponding questions in the"
                  , "# Debconf configuration, but not all of them."
                  , "#"
                  , "# This is a Debian specific file"
                  , ""
                  , "dc_eximconfig_configtype='" ++ show t ++ "'"
                  , "dc_other_hostnames='synchrotron-soleil.fr'"
                  , "dc_local_interfaces='127.0.0.1'"
                  , "dc_readhost='synchrotron-soleil.fr'"
                  , "dc_relay_domains=''"
                  , "dc_minimaldns='false'"
                  , "dc_relay_nets=''"
                  , "dc_smarthost='" ++ s ++"'"
                  , "CFILEMODE='644'"
                  , "dc_use_split_config='true'"
                  , "dc_hide_mailname='true'"
                  , "dc_mailname_in_oh='true'"
                  , "dc_localdelivery='mail_spool'"
                  ]
    installed :: Property DebianLike
    installed = Apt.reConfigure "exim4-config" []
                -- [ ("exim4/dc_eximconfig_configtype", "select", "satellite")
                -- , ("exim4/dc_other_hostnames", "string", "synchrotron-soleil.fr")
                -- , ("exim4/dc_local_interfaces", "string", "127.0.0.1")
                -- , ("exim4/dc_readhost", "string", "synchrotron-soleil.fr")
                -- , ("exim4/dc_relay_domains", "string", "")
                -- , ("exim4/dc_minimaldns", "boolean", "false")
                -- , ("exim4/dc_relay_nets", "string", "")
                -- , ("exim4/dc_smarthost", "string", "smtp.orange.fr")
                -- , ("exim4/use_split_config", "boolean", "true")
                -- , ("exim4/hide_mailname", "boolean", "true")
                -- , ("exim4/dc_localdelivery", "select", "mail_spool")
                -- ]

-- | mount ruche

data DoMount = DoMount | DoNotMount

data MountConf = MountConf Mount.FsType Mount.Source Mount.MountPoint Mount.MountOpts

data Beamline = Cristal | Diffabs | Hermes | Mars | Nanoscopium | Ode | Proxima1 | Rock | Sextants | Sixs

instance Show Beamline where
    show Cristal     = "cristal"
    show Diffabs     = "diffabs"
    show Hermes      = "hermes"
    show Mars        = "mars"
    show Nanoscopium = "nanoscopium"
    show Ode         = "ode"
    show Proxima1    = "proxima1"
    show Rock        = "rock"
    show Sextants    = "sextants"
    show Sixs        = "sixs"

mount' :: DoMount -> MountConf -> RevertableProperty Linux UnixLike
mount' now (MountConf fs src mnt opts) = mount <!> umount
  where
    mount :: Property Linux
    mount = case now of
              DoMount    -> Fstab.mounted fs src mnt opts
              DoNotMount -> tightenTargets $ Fstab.listed fs src mnt opts

    umount :: Property UnixLike
    umount = propertyList ("remove nfs " ++ mnt ++ "mount point") $ props
             & ((property "umount" $ do
                   liftIO $ Mount.unmountBelow mnt
                   return NoChange) :: Property UnixLike)
             -- `assume` NoChange
             & etcFstab `File.lacksLine` l

    l = intercalate "\t" [src, mnt, fs, Mount.formatMountOpts opts, dump, passno]

    dump = "0"

    passno = "2"

    etcFstab :: FilePath
    etcFstab = "/etc/fstab"

-- | A mount point for a filesystem.

data Fsc = NoFsCache | FsCache

cachefs :: MountConf -> Property Debian
cachefs conf = started
               `requires` mount' DoMount conf
               `requires` cachefilesd
               `requires` Apt.backportInstalled ["cachefilesd"]
  where
    started :: Property Linux
    started = Systemd.running "cachefilesd"

    cachefilesd :: Property UnixLike
    cachefilesd = "/etc/default/cachefilesd"
                  `File.hasContent`
                  ["# Defaults for cachefilesd initscript"
                  , "# sourced by /etc/init.d/cachefilesd"
                  , ""
                  , "# You must uncomment the run=yes line below for cachefilesd to start."
                  , "# Before doing so, please read /usr/share/doc/cachefilesd/howto.txt.gz as"
                  , "# extended user attributes need to be enabled on the cache filesystem."
                  , "RUN=yes"
                  , ""
                  , "# Additional options that are passed to the Daemon."
                  , "DAEMON_OPTS=\"\""
                  ]

mounts :: DoMount -> [MountConf] -> RevertableProperty Linux UnixLike
mounts now ls = mconcat (map (mount' now) ls)

rucheOpts :: Mount.MountOpts
rucheOpts = Mount.MountOpts [ "rw", "intr", "hard", "acdirmin=1", "vers=3", "local_lock=flock"]

rucheOpts2 :: Mount.MountOpts
rucheOpts2 = Mount.MountOpts [ "rw", "intr", "hard", "acdirmin=1", "vers=3", "local_lock=flock", "x-systemd.automount"]

rucheOpts3 :: Fsc -> Mount.MountOpts
rucheOpts3 fsc = Mount.MountOpts $
  case fsc of
    FsCache   -> opts ++ ["fsc"]
    NoFsCache -> opts
  where
    (Mount.MountOpts opts) = rucheOpts2

mountSoleil :: Beamline -> Fsc -> Versioned Int (RevertableProperty DebianLike DebianLike)
mountSoleil b fsc ver = ver (     (== 1) --> mounts DoMount ls `requires` installed
                              <|> (== 2) --> mounts DoMount ls2 `requires` installed
                              <|> (== 3) --> mounts DoMount ls3 `requires` installed
                            )
  where
    ls = [ MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/" ++ beamline ++ "-users") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-users") rucheOpts
         , MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/" ++ beamline ++ "-soleil") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-soleil") rucheOpts
         , MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/instrumentation") ("/nfs/ruche-" ++ beamline ++ "/instrumentation") rucheOpts
         , MountConf "nfs" "krypton.exp.synchrotron-soleil.fr:/FS_TMPEXP/share-temp" "/nfs/share-temp" mempty
         ]

    ls2 = [ MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/" ++ beamline ++ "-users") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-users") rucheOpts2
          , MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/" ++ beamline ++ "-soleil") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-soleil") rucheOpts2
          , MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/instrumentation") ("/nfs/ruche-" ++ beamline ++ "/instrumentation") rucheOpts2
          , MountConf "nfs" "krypton.exp.synchrotron-soleil.fr:/FS_TMPEXP/share-temp" "/nfs/share-temp" (Mount.MountOpts [ "x-systemd.automount" ])
          ]

    ls3 = [ MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/" ++ beamline ++ "-users") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-users") (rucheOpts3 fsc)
          , MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/" ++ beamline ++ "-soleil") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-soleil") (rucheOpts3 fsc)
          , MountConf "nfs" ("ruche-" ++ beamline ++ ".exp.synchrotron-soleil.fr:/instrumentation") ("/nfs/ruche-" ++ beamline ++ "/instrumentation") (rucheOpts3 fsc)
          , MountConf "nfs" "krypton.exp.synchrotron-soleil.fr:/FS_TMPEXP/share-temp" "/nfs/share-temp" (Mount.MountOpts [ "x-systemd.automount" ])
          ]

    beamline = show b

    installed :: RevertableProperty DebianLike DebianLike
    installed = Apt.installed ["nfs-common"] <!> (doNothing :: Property DebianLike)


mountSoleilEssaim :: Beamline -> Fsc -> Versioned Int (RevertableProperty DebianLike DebianLike)
mountSoleilEssaim b fsc ver = ver (     (== 1) --> mounts DoMount ls `requires` installed )
  where
    ls = [ MountConf "nfs" ("ruche-" ++ beamline ++ ".essaim.exp.synchrotron-soleil.fr:/" ++ beamline ++ "-users") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-users") (rucheOpts3 fsc)
         , MountConf "nfs" ("ruche-" ++ beamline ++ ".essaim.exp.synchrotron-soleil.fr:/" ++ beamline ++ "-soleil") ("/nfs/ruche-" ++ beamline ++ "/" ++ beamline ++ "-soleil") (rucheOpts3 fsc)
         , MountConf "nfs" ("ruche-" ++ beamline ++ ".essaim.exp.synchrotron-soleil.fr:/instrumentation") ("/nfs/ruche-" ++ beamline ++ "/instrumentation") (rucheOpts3 fsc)
         , MountConf "nfs" "krypton.exp.synchrotron-soleil.fr:/FS_TMPEXP/share-temp" "/nfs/share-temp" (Mount.MountOpts [ "x-systemd.automount" ])
         ]

    beamline = show b

    installed :: RevertableProperty DebianLike DebianLike
    installed = Apt.installed ["nfs-common"] <!> (doNothing :: Property DebianLike)

-- | Install apt sources and mentors

sources :: Property DebianLike
sources =  Apt.setSourcesListD [ "deb-src http://deb.debian.org/debian/ testing main contrib non-free"
                               , "deb-src http://deb.debian.org/debian/ unstable main contrib non-free"
                               , "deb-src http://deb.debian.org/debian/ experimental main contrib non-free"
                               ] "sources"

pniHdri :: Property DebianLike
pniHdri = Apt.setSourcesListD [ "deb-src [trusted=yes] http://repos.pni-hdri.de/apt/debian stretch main" ] "pni-hdri"

-- | Video

data Video = DefaultVideo
           | NVidiaVideo
           | NVidiaBackportedVideo

installVideo :: Video -> Property Debian
installVideo DefaultVideo = doNothing
installVideo NVidiaVideo = tightenTargets $ Apt.installed ["xserver-xorg-video-nvidia"]
installVideo NVidiaBackportedVideo = Apt.backportInstalled ["xserver-xorg-video-nvidia"]

-- | LDAP

ldap :: String -> Property DebianLike
ldap l =  withOS "Configuring LDAP Stack" $ \w o -> ensureProperty w $case o of
        (Just (System (Debian _ Unstable) _)) -> buster
        (Just (System (Debian _ (Stable "bullseye")) _)) -> bullseye
        (Just (System (Debian _ (Stable "buster")) _)) -> buster
        (Just (System (Debian _ (Stable "stretch")) _)) -> stretch
        (Just (System (Debian _ (Stable "jessie")) _)) -> stretch
        _ -> error $ "ldap installation not yet implemented on " ++ show o
    where
      stretch :: Property DebianLike
      stretch = propertyList "Stretch ldap" $ props
                & Apt.reConfigure "libnss-ldapd" [ ("libnss-ldapd/nsswitch", "multiselect", "group, passwd, shadow") ] `requires`  Apt.installed ["libnss-ldapd"]
                & File.containsBlock "/etc/nslcd.conf" [ "uri ldap://ldap.exp.synchrotron-soleil.fr/"
                                                       , "base dc=EXP" ] `onChange` Service.restarted "nslcd"
                & Apt.installed ["libpam-ldapd"]
                & File.containsBlock  "/etc/ldap/ldap.conf" [ "URI ldap://ldap.exp.synchrotron-soleil.fr"
                                                            , "BASE dc=EXP"
                                                            ]
                & File.hasContent "/usr/share/pam-configs/mkhomedir" [ "Name: Create home directory during login"
                                                                     , "Default: yes"
                                                                     , "Priority: 900"
                                                                     , "Session-Type: Additional"
                                                                     , "Session:"
                                                                     , "        required        pam_mkhomedir.so umask=0022 skel=/etc/skel"
                                                                     ]
                `onChange` (scriptProperty ["pam-auth-update --package"] `assume` MadeChange)

      buster :: Property DebianLike
      buster = propertyList "Buster ldap" $ props
               & Apt.reConfigure "libnss-ldapd" [ ("libnss-ldapd/nsswitch", "multiselect", "group, passwd, shadow") ] `requires`  Apt.installed ["libnss-ldapd"]
               & File.hasContent "/etc/nslcd.conf" [ "# DO NOT EDIT, MANAGED VIA PROPELLOR"
                                                   , " "
                                                   , "# /etc/nslcd.conf "
                                                   , "# nslcd configuration file. See nslcd.conf(5) "
                                                   , "# for details. "
                                                   , " "
                                                   , "# The user and group nslcd should run as. "
                                                   , "uid nslcd "
                                                   , "gid nslcd "
                                                   , " "
                                                   , "# The location at which the LDAP server(s) should be reachable. "
                                                   , "uri " ++ l --ldap://ldap.exp.synchrotron-soleil.fr/ "
                                                   , " "
                                                   , "# The search base that will be used for all queries. "
                                                   , "base dc=EXP "
                                                   , " "
                                                   , "# The LDAP protocol version to use. "
                                                   , "#ldap_version 3 "
                                                   , " "
                                                   , "# The DN to bind with for normal lookups. "
                                                   , "#binddn cn=annonymous,dc=example,dc=net "
                                                   , "#bindpw secret "
                                                   , " "
                                                   , "# The DN used for password modifications by root. "
                                                   , "#rootpwmoddn cn=admin,dc=example,dc=com "
                                                   , " "
                                                   , "# SSL options "
                                                   , "#ssl off "
                                                   , "#tls_reqcert never "
                                                   , "tls_cacertfile /etc/ssl/certs/ca-certificates.crt "
                                                   , " "
                                                   , "# The search scope. "
                                                   , "#scope sub "
                                                   , " "
                                                   , "# SOLEIL specific in order to avoid login with System User for everyone."
                                                   , "map passwd gecos givenName"
                                                   ]
               `onChange` Service.restarted "nslcd"
               `requires` Apt.installed [ "nslcd" ]
               & Apt.installed ["libpam-ldapd"]
               & File.containsBlock  "/etc/ldap/ldap.conf" [ "URI " ++ l -- ldap://ldap.exp.synchrotron-soleil.fr"
                                                           , "BASE dc=EXP"
                                                           ]
               & scriptProperty ["pam-auth-update --enable mkhomedir"] `assume` MadeChange
      bullseye :: Property DebianLike
      bullseye = propertyList "Bullseye ldap" $ props
               & Apt.reConfigure "libnss-ldapd" [ ("libnss-ldapd/nsswitch", "multiselect", "group, passwd, shadow") ] `requires`  Apt.installed ["libnss-ldapd"]
               & File.hasContent "/etc/nslcd.conf" [ "# DO NOT EDIT, MANAGED VIA PROPELLOR"
                                                   , " "
                                                   , "# /etc/nslcd.conf "
                                                   , "# nslcd configuration file. See nslcd.conf(5) "
                                                   , "# for details. "
                                                   , " "
                                                   , "# The user and group nslcd should run as. "
                                                   , "uid nslcd "
                                                   , "gid nslcd "
                                                   , " "
                                                   , "# The location at which the LDAP server(s) should be reachable. "
                                                   , "uri " ++ l --ldap://ldap.exp.synchrotron-soleil.fr/ "
                                                   , " "
                                                   , "# The search base that will be used for all queries. "
                                                   , "base dc=EXP "
                                                   , " "
                                                   , "# The LDAP protocol version to use. "
                                                   , "#ldap_version 3 "
                                                   , " "
                                                   , "# The DN to bind with for normal lookups. "
                                                   , "#binddn cn=annonymous,dc=example,dc=net "
                                                   , "#bindpw secret "
                                                   , " "
                                                   , "# The DN used for password modifications by root. "
                                                   , "#rootpwmoddn cn=admin,dc=example,dc=com "
                                                   , " "
                                                   , "# SSL options "
                                                   , "#ssl off "
                                                   , "#tls_reqcert never "
                                                   , "tls_cacertfile /etc/ssl/certs/ca-certificates.crt "
                                                   , " "
                                                   , "# The search scope. "
                                                   , "#scope sub "
                                                   , " "
                                                   , "# SOLEIL specific in order to avoid login with System User for everyone."
                                                   , "map passwd gecos givenName"
                                                   ]
               `onChange` Service.restarted "nslcd"
               `requires` Apt.installed [ "nslcd" ]
               & Apt.installed ["libpam-ldapd"]
               & File.hasContent "/etc/ldap/ldap.conf" [ "# DO NOT EDIT, MANAGED VIA PROPELLOR"
                                                       , "#"
                                                       , "# LDAP Defaults"
                                                       , "#"
                                                       , ""
                                                       , "# See ldap.conf(5) for details"
                                                       , "# This file should be world readable but not world writable."
                                                       , ""
                                                       , "BASE   dc=EXP"
                                                       , "URI    " ++ l
                                                       , ""
                                                       , "#SIZELIMIT      12"
                                                       , "#TIMELIMIT      15"
                                                       , "#DEREF          never"
                                                       , ""
                                                       , "# TLS certificates (needed for GnuTLS)"
                                                       , "TLS_CACERT      /etc/ssl/certs/ca-certificates.crt"
                                                       ]
               & scriptProperty ["pam-auth-update --enable mkhomedir"] `assume` MadeChange

-- | Packages

hklDev :: Property DebianLike
hklDev = propertyList "installed all the hkl dev dependencies" $ props
         & Apt.installed [ "asymptote"
                         , "emacs-goodies-el"
                         , "glade"
                         , "gnuplot-mode"
                         , "libghc-aeson-prof"
                         , "libghc-attoparsec-prof"
                         , "libghc-bindings-dsl-dev"
                         , "libghc-conduit-doc"
                         , "libghc-conduit-prof"
                         , "libghc-dimensional-prof"
                         , "libghc-errors-prof"
                         , "libghc-fgl-prof"
                         , "libghc-glob-prof"
                         , "libghc-gtk-prof"
                         , "libghc-hmatrix-prof"
                         , "libghc-hmatrix-gsl-prof"
                         , "libghc-juicypixels-prof"
                         , "libghc-lens-prof"
                         , "libghc-lifted-async-prof"
                         , "libghc-lifted-base-prof"
                         , "libghc-monad-control-prof"
                         , "libghc-monad-loops-prof"
                         , "libghc-monads-tf-prof"
                         , "libghc-optparse-applicative-prof"
                         , "libghc-path-prof"
                         , "libghc-path-io-prof"
                         , "libghc-pipes-prof"
                         , "libghc-pipes-safe-prof"
                         , "libghc-puremd5-prof"
                         , "libghc-repa-prof"
                         , "libghc-terminal-progress-bar-prof"
                         , "libghc-terminal-progress-bar-doc"
                         , "libgoffice-0.10-dev"
                         , "c2hs"
                         ]



autoprocessingDev :: Property DebianLike
autoprocessingDev = propertyList "installed all the autoprocessing dev dependencies" $ props
                    & Apt.installed [ "libghc-cassava-prof"
                                    , "libghc-curl-prof"
                                    , "libghc-dimensional-prof"
                                    , "libghc-extra-prof"
                                    , "libghc-generic-deriving-prof"
                                    , "libghc-http-types-prof"
                                    , "libghc-hxt-prof"
                                    , "libghc-lens-prof"
                                    , "libghc-missingh-prof"
                                    , "libghc-path-prof"
                                    , "libghc-path-io-prof"
                                    , "libghc-servant-client-prof"
                                    , "libghc-servant-server-prof"
                                    , "libghc-shake-prof"
                                    , "libghc-soap-prof"
                                    , "libghc-temporary-prof"
                                    , "libghc-wai-extra-prof"
                                    , "libghc-xml-prof"
                                      -- doc
                                    , "libghc-missingh-doc"
                                    , "libghc-aeson-doc"
                                    , "libghc-cassava-doc"
                                    , "libghc-dimensional-doc"
                                    , "libghc-extra-doc"
                                    , "libghc-generic-deriving-doc"
                                    , "libghc-http-client-doc"
                                    , "libghc-http-types-doc"
                                    , "libghc-hxt-doc"
                                    , "libghc-lens-prof"
                                    , "libghc-missingh-doc"
                                    , "libghc-optparse-applicative-doc"
                                    , "libghc-path-doc"
                                    , "libghc-path-io-doc"
                                    , "libghc-servant-client-doc"
                                    , "libghc-servant-server-doc"
                                    , "libghc-shake-doc"
                                    , "libghc-soap-doc"
                                    , "libghc-stm-doc"
                                    , "libghc-temporary-doc"
                                    , "libghc-wai-extra-doc"
                                    , "libghc-warp-doc"
                                    ]

binocularsLatest :: Property Debian
binocularsLatest = do
  let p ="binoculars"
  let url = "https://github.com/picca/binoculars"
  let ps = ["binoculars", "python3-binoculars", "python-binoculars-doc"]
  let buster = Soleil.Gbp url (Just "master") (Just "--git-upstream-tag=HEAD") p

  check (not <$> Apt.isInstalled' ps) $ withOS ("Install " ++ p ++ " from: " ++ url) $ \w o ->
    ensureProperty w $ Soleil.buildAndInstall $
    case o of
      (Just (System (Debian _ (Stable "buster")) _)) -> buster
      (Just (System (Debian _ Unstable) _)) -> buster
      _ -> error $ p ++ " installation not yet implemented on " ++ show o

hsHdf5 :: Property DebianLike
hsHdf5 = do
  let ps = [ "libghc-hdf5-dev"
           , "libghc-hdf5-prof"
           , "libghc-hdf5-doc"
           ]
  check (not <$> Apt.isInstalled' ps) $
            ( propertyList "Build and install hs-hdf5" $ props
            & Soleil.buildAndInstall (Soleil.Git "https://github.com/picca/hs-hdf5" Nothing "hs-hdf5"))

hklHs :: Property DebianLike
hklHs = do
  let ps = [ "haskell-hkl-utils" ]
  check (not <$> Apt.isInstalled' ps) $
            ( propertyList "Build and install the the hkl haskell part" $ props
            & hsHdf5
            & Soleil.buildAndInstall (Soleil.GitWithSubDir "https://repo.or.cz/hkl.git" (Just "master") "haskell-hkl" "contrib/haskell"))

pynx :: Maybe Git.Branch -> Property (HasInfo + DebianLike)
pynx mb = do
  let ps = ["pynx"]
  check (not <$> Apt.isInstalled' ps) $ withPrivData (Password "gitlab.esrf.fr/picca/PyNX.git") (Context "gitlab.esrf.fr/picca/PyNX.git") $ \getdata ->
    property' "gitlab.esrf.fr pynx" $ \w -> getdata $ \privdata -> ensureProperty w
    (Soleil.buildAndInstall (Soleil.Git ("https://picca:" ++ (privDataVal privdata) ++ "@gitlab.esrf.fr/picca/PyNX.git") mb "pynx"))


pypi :: Package -> Maybe String -> Property DebianLike
pypi p o = check (not <$> Apt.isInstalled' ["python3-" ++ p]) $
           Soleil.buildAndInstall (Soleil.Pypi p o)

scikitCuda :: Property DebianLike
scikitCuda = pypi "scikit-cuda" (Just "nocheck")

gprof2dot :: Property DebianLike
gprof2dot = pypi "gprof2dot" Nothing

hdf5plugin :: Property DebianLike
hdf5plugin = pypi "hdf5plugin" Nothing

kkcalc :: Property DebianLike
kkcalc = pypi "kkcalc" (Just "nocheck")

asteval_bpo :: Property DebianLike
asteval_bpo = do
  let ps = ["python3-asteval", "python-asteval-doc"]
  check (not <$> Apt.isInstalled' ps) $
    ( propertyList "Build and install python-asteval bpo" $ props
      & Soleil.buildAndInstall (Soleil.Backport "python-asteval" Nothing Nothing))

lmfit_bpo :: Property DebianLike
lmfit_bpo = do
  let ps = ["python3-lmfit", "python-lmfit-doc"]
  check (not <$> Apt.isInstalled' ps) $
    ( propertyList "Build and install lmfit-py bpo" $ props
      & asteval_bpo
      & Soleil.buildAndInstall (Soleil.Backport "lmfit-py" Nothing Nothing))

xrayutilities_bpo :: Property DebianLike
xrayutilities_bpo = do
  let ps = ["python3-xrayutilities", "python-xrayutilities-doc"]
  check (not <$> Apt.isInstalled' ps) $
    ( propertyList "Build and install xrayutilities" $ props
      & Soleil.buildAndInstall (Soleil.Backport "xrayutilities" (Just "'nocheck nodocs'") Nothing))

bcdi :: (Maybe Git.Branch) -> Property (HasInfo + DebianLike)
bcdi mb = do
  let ps = ["python3-bcdi", "python-bcdi-doc"]
  check (not <$> Apt.isInstalled' ps) $
    ( propertyList "build and install bcdi" $ props
      & pynx mb
      -- & xrayutilities_bpo
      & Soleil.buildAndInstall (Soleil.Gbp "https://salsa.debian.org/science-team/bcdi" Nothing Nothing "bcdi"))

xsocs :: Property DebianLike
xsocs = Soleil.buildAndInstall (Soleil.Git "https://gitlab.esrf.fr/picca/xsocs" Nothing "xsoxs")

dials :: Property DebianLike
dials = do
  let ps = ["dials"]
  check (not <$> Apt.isInstalled' ps) $
            ( propertyList "Build and install dials" $ props
            & Soleil.buildAndInstall (Soleil.Backport "cbflib" Nothing Nothing)
            & Soleil.buildAndInstall (Soleil.Gbp "https://salsa.debian.org/science-team/dials" Nothing Nothing "dials"))

pyhst2 :: Property DebianLike
pyhst2 = do
  let ps = ["python3-pyhst2-cuda"]
  check (not <$> Apt.isInstalled' ps) $
            ( propertyList "Build and install dials" $ props
            & Soleil.buildAndInstall (Soleil.Backport "pyhst2" Nothing Nothing))

distUpgrade :: String -> Property DebianLike
distUpgrade p = combineProperties ("apt " ++ p) $ props
	& Apt.pendingConfigured
	& Apt.runApt ["-y", "--force-yes", "-o", "Dpkg::Options::=--force-confnew", p]
		`assume` MadeChange

-- | Proxy setup

type Proxy = Url

proxySetup :: Proxy -> Property UnixLike
proxySetup proxy = "/etc/environment" `File.hasContent` [ "http_proxy=" ++ proxy
                                                        , "HTTP_PROXY=" ++ proxy
                                                        , "https_proxy=" ++ proxy
                                                        , "HTTPS_PROXY=" ++ proxy
                                                        , "ftp_proxy=" ++ proxy
                                                        , "FTP_PROXY=" ++ proxy
                                                        , "NO_PROXY=.synchrotron-soleil.fr,.exp.synchrotron-soleil.fr"
                                                        , "no_proxy=.synchrotron-soleil.fr,.exp.synchrotron-soleil.fr"
                                                        ]

-- | NTP

type NTPServer = String

ntp :: NTPServer -> Property DebianLike
ntp server =  withOS "Install firefoxProxy" $ \w o ->
              case o of
                (Just (System (Debian _ (Stable "bullseye")) _)) -> ensureProperty w $ buster
                (Just (System (Debian _ (Stable "buster")) _)) -> ensureProperty w $ buster
                _ -> error $ "NTP server setting is not support on " ++ show o
  where
    buster = File.hasContent "/etc/ntp.conf" content
             `onChange` Service.restarted "ntp"
             `requires` Apt.serviceInstalledRunning "ntp"
             `describe` (server ++ " as ntp server")

    content = [ "# DO NOT EDIT -- MANAGED VIA PROPELLOR"
              , "# /etc/ntp.conf, configuration for ntpd; see ntp.conf(5) for help"
              , ""
              , "driftfile /var/lib/ntp/ntp.drift"
              , ""
              , "# Leap seconds definition provided by tzdata"
              , "leapfile /usr/share/zoneinfo/leap-seconds.list"
              , ""
              , "# Enable this if you want statistics to be logged."
              , "#statsdir /var/log/ntpstats/"
              , ""
              , "statistics loopstats peerstats clockstats"
              , "filegen loopstats file loopstats type day enable"
              , "filegen peerstats file peerstats type day enable"
              , "filegen clockstats file clockstats type day enable"
              , ""
              , ""
              , "# You do need to talk to an NTP server or two (or three)."
              , "server " ++ server
              , ""
              , "# pool.ntp.org maps to about 1000 low-stratum NTP servers.  Your server will"
              , "# pick a different set every time it starts up.  Please consider joining the"
              , "# pool: <http://www.pool.ntp.org/join.html>"
              , "#pool 0.debian.pool.ntp.org iburst"
              , "#pool 1.debian.pool.ntp.org iburst"
              , "#pool 2.debian.pool.ntp.org iburst"
              , "#pool 3.debian.pool.ntp.org iburst"
              , ""
              , ""
              , "# Access control configuration; see /usr/share/doc/ntp-doc/html/accopt.html for"
              , "# details.  The web page <http://support.ntp.org/bin/view/Support/AccessRestrictions>"
              , "# might also be helpful."
              , "#"
              , "# Note that \"restrict\" applies to both servers and clients, so a configuration"
              , "# that might be intended to block requests from certain clients could also end"
              , "# up blocking replies from your own upstream servers."
              , ""
              , "# By default, exchange time with everybody, but don't allow configuration."
              , "restrict -4 default kod notrap nomodify nopeer noquery limited"
              , "restrict -6 default kod notrap nomodify nopeer noquery limited"
              , ""
              , "# Local users may interrogate the ntp server more closely."
              , "restrict 127.0.0.1"
              , "restrict ::1"
              , ""
              , "# Needed for adding pool entries"
              , "restrict source notrap nomodify noquery"
              , ""
              , "# Clients from this (example!) subnet have unlimited access, but only if"
              , "# cryptographically authenticated."
              , "#restrict 192.168.123.0 mask 255.255.255.0 notrust"
              , ""
              , ""
              , "# If you want to provide time to your local subnet, change the next line."
              , "# (Again, the address is an example only.)"
              , "#broadcast 192.168.123.255"
              , ""
              , "# If you want to listen to time broadcasts on your local subnet, de-comment the"
              , "# next lines.  Please do this only if you trust everybody on the network!"
              , "#disable auth"
              , "#broadcastclient"
              ]

-- firefoxProxy

data FValue = FString String
            | FInt Int
            | FBool Bool

data FirefoxProperty = FPref String FValue

asLine :: FirefoxProperty -> String
asLine (FPref k (FString v))   = "pref(\"" ++ k ++ "\", \"" ++ v ++ "\");"
asLine (FPref k (FInt v))      = "pref(\"" ++ k ++ "\", " ++ show v ++ ");"
asLine (FPref k (FBool True))  = "pref(\"" ++ k ++ "\", true);"
asLine (FPref k (FBool False)) = "pref(\"" ++ k ++ "\", false);"

firefoxProxy :: [FirefoxProperty] -> Property UnixLike
firefoxProxy ps = withOS "Install firefoxProxy" $ \w o -> case o of
  (Just (System (Debian _ (Stable "bullseye")) _)) -> ensureProperty w $ stretch
  (Just (System (Debian _ (Stable "buster")) _)) -> ensureProperty w $ stretch
  (Just (System (Debian _ (Stable "stretch")) _)) -> ensureProperty w $ stretch
  (Just (System (Debian _ (Stable "jessie")) _)) -> ensureProperty w $ jessie
  _ -> error $ "Firefox proxy setting is not support on " ++ show o
  where
    jessie = propertyList "Setting firefox-esr proxy" $ props
             & File.dirExists "/etc/iceweasel/profile/"
             & File.hasContent "/etc/iceweasel/profile/prefs.js" (map asLine ps)
    stretch =  propertyList "Setting firefox-esr proxy" $ props
               & File.notPresent "/etc/firefox-esr/profile/prefs.js"
               & File.notPresent "/etc/firefox-esr/profile"
               & File.hasContent "/etc/firefox-esr/soleil-prefs.js" (map asLine ps)


-- | gdm

gdmConfig :: Property UnixLike
gdmConfig = withOS "Configured gdm" $ \w o -> case o of
  (Just (System (Debian _ (Stable "bullseye")) _)) -> ensureProperty w $ bullseye
  (Just (System (Debian _ (Stable "buster")) _)) -> ensureProperty w $ buster
  _ -> error $ "gdm configuratin is not supported on " ++ show o
  where
    bullseye = File.hasContent "/etc/gdm3/greeter.dconf-defaults"
               [ "# These are the options for the greeter session that can be set "
               , "# through GSettings. Any GSettings setting that is used by the "
               , "# greeter session can be set here."
               , ""
               , "# Note that you must configure the path used by dconf to store the "
               , "# configuration, not the GSettings path."
               , ""
               , ""
               , "# Theming options"
               , "# ==============="
               , "#  - Change the GTK+ theme"
               , "[org/gnome/desktop/interface]"
               , "# gtk-theme='Adwaita'"
               , "#  - Use another background"
               , "[org/gnome/desktop/background]"
               , "# picture-uri='file:///usr/share/themes/Adwaita/backgrounds/stripes.jpg'"
               , "# picture-options='zoom'"
               , "#  - Or no background at all"
               , "[org/gnome/desktop/background]"
               , "# picture-options='none'"
               , "# primary-color='#000000'"
               , ""
               , "# Login manager options"
               , "# ====================="
               , "[org/gnome/login-screen]"
               , "logo='/usr/share/images/vendor-logos/logo-text-version-128.png'"
               , ""
               , "# - Disable user list"
               , "disable-user-list=true"
               , "# - Disable restart buttons"
               , "# disable-restart-buttons=true"
               , "# - Show a login welcome message"
               , "banner-message-enable=true"
               , "banner-message-text='Beware: no backup on this computer'"
               , ""
               , "# Automatic suspend"
               , "# ================="
               , "[org/gnome/settings-daemon/plugins/power]"
               , "# - Time inactive in seconds before suspending with AC power"
               , "#   1200=20 minutes, 0=never"
               , "sleep-inactive-ac-timeout=0"
               , "# - What to do after sleep-inactive-ac-timeout"
               , "#   'blank', 'suspend', 'shutdown', 'hibernate', 'interactive' or 'nothing'"
               , "sleep-inactive-ac-type='blank'"
               , "# - As above but when on battery"
               , "# sleep-inactive-battery-timeout=1200"
               , "# sleep-inactive-battery-type='suspend'"
               ]
    buster = File.hasContent "/etc/gdm3/greeter.dconf-defaults"
             [ "# These are the options for the greeter session that can be set "
             , "# through GSettings. Any GSettings setting that is used by the "
             , "# greeter session can be set here."
             , ""
             , "# Note that you must configure the path used by dconf to store the "
             , "# configuration, not the GSettings path."
             , ""
             , ""
             , "# Theming options"
             , "# ==============="
             , "#  - Change the GTK+ theme"
             , "[org/gnome/desktop/interface]"
             , "# gtk-theme='Adwaita'"
             , "#  - Use another background"
             , "[org/gnome/desktop/background]"
             , "# picture-uri='file:///usr/share/themes/Adwaita/backgrounds/stripes.jpg'"
             , "# picture-options='zoom'"
             , "#  - Or no background at all"
             , "[org/gnome/desktop/background]"
             , "# picture-options='none'"
             , "# primary-color='#000000'"
             , ""
             , "# Login manager options"
             , "# ====================="
             , "[org/gnome/login-screen]"
             , "logo='/usr/share/images/vendor-logos/logo-text-version-128.png'"
             , ""
             , "# - Disable user list"
             , "disable-user-list=true"
             , "# - Disable restart buttons"
             , "# disable-restart-buttons=true"
             , "# - Show a login welcome message"
             , "banner-message-enable=true"
             , "banner-message-text='Beware: no backup on this computer'"
             , ""
             , "# Automatic suspend"
             , "# ================="
             , "[org/gnome/settings-daemon/plugins/power]"
             , "# - Time inactive in seconds before suspending with AC power"
             , "#   1200=20 minutes, 0=never"
             , "# sleep-inactive-ac-timeout=1200"
             , "# - What to do after sleep-inactive-ac-timeout"
             , "#   'blank', 'suspend', 'shutdown', 'hibernate', 'interactive' or 'nothing'"
             , "# sleep-inactive-ac-type='suspend'"
             , "# - As above but when on battery"
             , "# sleep-inactive-battery-timeout=1200"
             , "# sleep-inactive-battery-type='suspend'"
             ]

-- | emacs

emacsConfig :: [File.Line]
emacsConfig =
  [ ";; DO NOT EDIT -- MANAGED BY PROPELLOR"
  , "(custom-set-variables"
  , " ;; custom-set-variables was added by Custom."
  , " ;; If you edit it by hand, you could mess it up, so be careful."
  , " ;; Your init file should contain only one such instance."
  , " ;; If there is more than one, they won't work right."
  , " '(after-init-hook (quote global-company-mode))"
  , " '(before-save-hook (quote (copyright-update delete-trailing-whitespace)))"
  , " '(c-default-style"
  , "   (quote"
  , "    ((c-mode . \"linux\")"
  , "     (c++-mode . \"linux\")"
  , "     (java-mode . \"java\")"
  , "     (awk-mode . \"awk\")"
  , "     (other . \"gnu\"))))"
  , " '(gnus-select-method (quote (nnimap \"lettres.chocolatnoir.net\")))"
  -- , " '(haskell-compile-cabal-build-command \"cabal v1-build --ghc-option=-ferror-spans\")"
  , " '(haskell-font-lock-symbols t)"
  , " '(haskell-mode-hook (quote (flyspell-prog-mode haskell-indent-mode)))"
  , " '(haskell-stylish-on-save t)"
  , " '(indent-tabs-mode nil)"
  , " '(inhibit-startup-screen t)"
  , " '(org-babel-load-languages (quote ((emacs-lisp . t) (shell . t) (python . t))))"
  , " '(org-babel-python-command \"python3\")"
  , " '(org-confirm-babel-evaluate nil)"
  , " '(org-export-with-sub-superscripts nil)"
  , " '(org-latex-listings (quote minted))"
  , " '(org-latex-minted-langs (quote ((emacs-lisp \"common-lisp\") (cc \"c++\") (cperl \"perl\") (shell-script \"bash\") (caml \"ocaml\") (python \"python\") (conf \"cfg\"))))"
  , " '(org-export-latex-minted-options '((\"frame\" \"lines\") (\"fontsize\" \"\\\\scriptsize\") (\"linenos\" \"\")))"
  , " '(org-latex-pdf-process  (quote (\"pdflatex -interaction nonstopmode --shell-escape -output-directory %o %f\" \"pdflatex -interaction nonstopmode --shell-escape -output-directory %o %f\" \"pdflatex -interaction nonstopmode --shell-escape -output-directory %o %f\")))"
  , " '(org-modules '(ol-bbdb ol-bibtex ol-docview ol-eww ol-gnus ol-info ol-irc ol-mhe ol-rmail org-tempo ol-w3m))"
  , " '(org-src-fontify-natively t)"
  , " '(pylint-command \"pylint3\")"
  , " '(python-check-command \"/usr/bin/pyflakes3\")"
  , " '(python-flymake-command (quote (\"pyflakes3\")))"
  , " '(python-shell-interpreter \"python3\")"
  , " '(send-mail-function (quote smtpmail-send-it))"
  , " '(show-trailing-whitespace t)"
  , " '(smtpmail-smtp-server \"lettres.chocolatnoir.net\")"
  , " '(smtpmail-smtp-service 25)"
  , " '(user-mail-address \"picca@debian.org\"))"
  , "(custom-set-faces"
  , " ;; custom-set-faces was added by Custom."
  , " ;; If you edit it by hand, you could mess it up, so be careful."
  , " ;; Your init file should contain only one such instance."
  , " ;; If there is more than one, they won't work right."
  , " )"
  , "(require 'powerline)"
  , "(powerline-default-theme)"
  ]

gitGlobalConfig :: [File.Line]
gitGlobalConfig =
  [ "[push]"
  , "  default = simple"
  , "[user]"
  , "  email = picca@synchrotron-soleil.fr"
  , "  name = Picca Frédéric-Emmanuel"
  , "[gui]"
  , "  stageuntracked = no"
  , "[pull]"
  , "  ff = only"
  , ""
  , "[url \"ssh://git@salsa.debian.org/\"]"
  , "  insteadOf = https://salsa.debian.org/"
  -- , ""
  -- , "[url \"ssh://git@gitlab.esrf.fr/\"]"
  -- , "  insteadOf = https://gitlab.esrf.fr/"
  ]

mrconfig :: [File.Line]
mrconfig = library ++ concat ( esrf "picca" ["PyNX"]
                    <> esrf "favre" ["PyNX"]
                    <> github "silx-kit" ["fabio", "pyFAI", "silx"]
                    <> github "alexmarie78" ["nexVisu"]
                    <> github "carnisj" ["bcdi"]
                    <> github "dkriegner" ["xrayutilities"]
                    <> github "Hirrolot" ["datatype99", "metalang99"]
                    <> github "kif" ["pyfai"]
                    <> github "picca" ["binoculars", "hs-hdf5"]
                    <> github "romangrothausmann" ["FacetAnalyser"]
                    <> mrgit "src/repo.or.cz" "ssh://repo.or.cz" Nothing ["hkl"]
                    <> mrgit "src/git.spwhitton.name" "https://git.spwhitton.name/" Nothing ["consfigurator"]
                    <> salsa "blends-team" ["pan"]
                    <> salsa "debian" [ "python-uncertainties"]
                    <> salsa "picca" ["anarod", "autoprocess-exe", "h5py"]
                    <> salsa "opencl-team" ["python-pyopencl"]
                    <> salsa "python-team/modules" [ "jupyter-sphinx"
                                                   , "python-fisx"
                                                   , "python-qtconsole"
                                                   ]
                    <> salsa "science-team" [ "astra-toolbox"
                                            , "bcdi"
                                            , "bornagain"
                                            , "denss"
                                            , "dials"
                                            , "dials-data"
                                            , "genx"
                                            , "gpyfft"
                                            , "guidata"
                                            , "guiqwt"
                                            , "hkl"
                                            , "hyperspy"
                                            , "libccp4"
                                            , "lmfit-py"
                                            , "mmdb"
                                            , "pyfai"
                                            , "pyhst2"
                                            , "pymca"
                                            , "pytango"
                                            , "python-fabio"
                                            , "python-param"
                                            , "python-procrunner"
                                            , "python-qwt"
                                            , "python-xrayutilities"
                                            , "sardana"
                                            , "silx"
                                            , "ssm"
                                            , "tango"
                                            , "ufo-core"
                                            , "ufo-filters"
                                            , "vitables"
                                            ]
                  )
  where
    library = [ "[DEFAULT]"
              , "lib="
              , "      msg () {"
              , "        echo \"I: $1:$2\""
              , "      }"
              , "      git_checkout () {"
              , "        git clone $1 $2 &&"
              , "        cd $MR_REPO &&"
              , "        { git branch --track upstream remotes/origin/upstream || true; } &&"
              , "        { git branch --track pristine-tar remotes/origin/pristine-tar || true; } &&"
              , "        { (echo  \"[DEFAULT]\"; echo \"pristine-tar = True\") > .git/gbp.conf; } &&"
              , "        { test -z \"$DEBFULLNAME\" || git config user.name \"$DEBFULLNAME\" || true; } &&"
              , "        { test -z \"$DEBEMAIL\" || git config user.email \"$DEBEMAIL\" || true; }"
              , "      }"
              , "     git_update () {"
              , "       CURRENT_BRANCH=` git rev-parse --abbrev-ref HEAD` &&"
              , "       { git fetch || true; } &&"
              , "       { for branch in `git for-each-ref --format=\"%(refname:lstrip=2)\" refs/heads` ; do git checkout $branch ; git pull ; done || true; } &&"
              , "       { git checkout $CURRENT_BRANCH || true; }"
               , "     }"
              ]

    mrgit :: String -> String -> Maybe String -> [String] -> [[String]]
    mrgit target url team modules = flip map modules $
      \m -> [ ""
           , case team of
               (Just t) -> "[" ++ target ++ "/" ++ t ++ "/" ++ m ++ "]"
               Nothing  -> "["  ++ target ++ "/" ++ m ++ "]"
           , case team of
               (Just t) -> "checkout = git clone '" ++ url ++ "/" ++ t ++ "/" ++ m ++ ".git' '" ++ m ++ "'"
               Nothing  -> "checkout = git clone '" ++ url ++ "/" ++ m ++ ".git' '" ++ m ++ "'"
           ]

    mrgit2 :: String -> String -> String -> [String] -> [[String]]
    mrgit2 target url team modules = flip map modules $
                          \ m -> [ ""
                                , "[" ++ target ++ "/" ++ team ++ "/" ++ m ++ "]"
                                , "checkout = git_checkout " ++ url ++ "/" ++ team ++ "/" ++ m ++ " " ++ m
                                , "update = git_update " ++ url ++ "/" ++ team ++ "/" ++ m ++  " " ++ m
                                , "skip=lazy"
                                ]
    salsa :: String -> [String] -> [[String]]
    salsa t = mrgit2 "debian" "https://salsa.debian.org" t

    github :: String -> [String] -> [[String]]
    github t = mrgit "src/github.com" "https://github.com" (Just t)

    esrf :: String -> [String] -> [[String]]
    esrf t = mrgit "src/gitlab.esrf.fr" "https://gitlab.esrf.fr" (Just t)


quiltConfig :: [File.Line]
quiltConfig =
    [ "d=. ; while [ ! -d $d/debian -a `readlink -e $d` != / ]; do d=$d/..; done"
    , "if [ -d $d/debian ] && [ -z $QUILT_PATCHES ]; then"
    , "        # if in Debian packaging tree with unset $QUILT_PATCHES"
    , "        QUILT_PATCHES=\"debian/patches\""
    , ""
    , "        if ! [ -d $d/debian/patches ]; then mkdir $d/debian/patches; fi"
    , "fi"
    ]

-- | aliases

aliases :: [File.Line]
aliases = [ "# DO NOT EDIT, MANAGED VIA PROPELLOR"
          , ""
          , "alias journal='emacs /ssh:picca@people.debian.org:journal.org'"
          ]

-- | ssh (agent forwarding for my nitrokey)

sshConfig :: [File.Line]
sshConfig = [ "# DO NOT EDIT, MANAGED VIA PROPELLOR"
            , ""
            , "RemoteForward /run/user/22503/gnupg/S.gpg-agent /run/user/22503/gnupg/S.gpg-agent.extra"
            ]

bashrc :: [File.Line]
bashrc = [ "#DO NOT EDIT MANAGED BY PROPELLOR"
         , ""
         , "if [ -f \"/etc/skel/.bashrc\" ]; then"
         , ". \"/etc/skel/.bashrc\""
         , "fi"
         , ""
         , "# configure gpg"
         , "export GPG_TTY=$(tty)"
         , ""
         , "# Launch gpg-agent"
         , "gpg-connect-agent /bye"
         , ""
         , "# When using SSH support, use the current TTY for passphrases prompts"
         , "gpg-connect-agent updatestartuptty /bye > /dev/null"
         , ""
         , "# Point the SSH_AUTH_SOCK to the one handled by gpg-agent"
         , "unset SSH_AGENT_PID"
         , "if [ \"${gnupg_SSH_AUTH_SOCK_by:-0}\" -ne $$ ]; then"
         , "  export SSH_AUTH_SOCK=\"$(gpgconf --list-dirs agent-ssh-socket)\""
         , "fi"
         , ""
         , "export DEBEMAIL=\"picca@debian.org\""
         , "export DEBFULLNAME=\"Picca Frédéric-Emmanuel\""
         ]
-- | TODO

-- Docker

   -- sudo mkdir -p /etc/systemd/system/docker.service.d
   -- sudo nano /etc/systemd/system/docker.service.d/https_proxy.conf
   -- sudo nano /etc/systemd/system/docker.service.d/http_proxy.conf
   -- [Service]
   -- Environment="HTTP_PROXY=http://195.221.0.35:8080/"

   -- sudo systemctl daemon-reload
   -- sudo systemctl restart docker
   -- sudo docker run hello-world

-- | DoNotSuspend

doNotSuspend :: Property Linux
doNotSuspend = propertyList "Do not suspend" $ props
  -- do not sleep
  & Systemd.masked "sleep.target"
  & Systemd.masked "suspend.target"
  & Systemd.masked "hibernate.target"
  & Systemd.masked "hybrid-sleep.target"

-- | MySetup

mySetup :: User -> Property UnixLike
mySetup u = propertyList ("Setup user: " ++ show u) $ props
  & Soleil.gpgUserConfig (GpgKeyId "0x5632906F4696E015") u
  & Soleil.bashrcConfig u bashrc
  & Soleil.emacsUserConfig u emacsConfig
  & Soleil.gitGlobalConfig u gitGlobalConfig
  & Soleil.withUserHomeHasContent ".mrconfig" u mrconfig
  & Soleil.withUserHomeHasContent ".bash_aliases" u aliases

-- | Personal Computers

homeSystem :: DebianSuite -> Architecture -> Motd -> Property (HasInfo + Debian)
homeSystem suite arch motd = propertyList "Home system" $ props
  -- & proxySetup proxy
  & standardSystem suite arch motd (Group "picca")
  & hklDev
  -- & hklHs
  & Apt.unattendedUpgrades
  & packages ["libdvd-pkg", "privoxy"]
  & backports []
  & Soleil.rra
  & mySetup (User "picca")
  & Soleil.withUserHomeHasContent ".ssh/config" (User "picca")
        [ "# DO NOT EDIT -- MANAGED BY PROPELLOR"
        , "Host grades-01"
        , "     HostName ta.ipanema-remote.fr"
        , "     Ciphers aes256-gcm@openssh.com,aes256-ctr"
        , "     Port 47840"
        , ""
        , "Host grades-01.synchrotron-soleil.fr"
        , "     HostName ta.ipanema-remote.fr"
        , "     Ciphers aes256-gcm@openssh.com,aes256-ctr"
        , "     Port 47840"
        , ""
        , "Host re-grades-01"
        , "     HostName ta.ipanema-remote.fr"
        , "     Ciphers aes256-gcm@openssh.com,aes256-ctr"
        , "     Port 47841"
        , ""
        , "Host re-grades-01.exp.synchrotron-soleil.fr"
        , "     HostName ta.ipanema-remote.fr"
        , "     Ciphers aes256-gcm@openssh.com,aes256-ctr"
        , "     Port 47841"
        ]

  & Cron.runPropellor (Cron.Times "0 8 * * *")
  -- & redirectRoot "picca@synchrotron-soleil.fr" "smtp.sfr.fr" Satellite
  where
    proxy = "http://127.0.0.1:8118"

cush :: Host
cush = host "cush.chocolatnoir.net" $ props
  & homeSystem (Stable "bullseye") X86_64 [ "Welcome to cush!" ]
  & Apt.useLocalCacher
  -- & redirectRoot "picca@synchrotron-soleil.fr" "smtp.sfr.fr" Smarthost
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian Unstable X86_64
      & Sbuild.useHostProxy cush
      & Sbuild.update `period` Weekly (Just 1))
  & Sbuild.userConfig (User "picca")
  & Sbuild.usableBy (User "picca")
  & hklDev

mordor :: Host
mordor = host "mordor" $ props
  & homeSystem Unstable X86_32 [ "Welcome to mordor!" ]
  & Network.cleanInterfacesFile
  & Network.dhcp "eth0"
  & Apt.installed (wm Xmonad)
  & sources
  & pniHdri
  & sbuild X86_32 (User "picca") mordor

pupuce :: Host
pupuce = host "pupuce" $ props
  & homeSystem Unstable X86_32 [ "Welcom to pupuce!" ]
  & Apt.installed (wm Xmonad)
  & sbuild X86_32 (User "picca") pupuce

ssd :: Host
ssd = host "2a02-8420-6c55-6500-d012-4688-0bee-a0c6.rev.sfr.net" $ props
  & homeSystem Unstable X86_64 [ "Welcome to the marvelous ssd!" ]
  & Apt.useLocalCacher
  & Apt.installed (wm Gnome)
  & sbuild X86_64 (User "picca") ssd
  & hklDev
  & hklHs
  & Soleil.dhtChroot (User "picca") X86_64


-- | Soleil computers RS

resSystem :: DebianSuite -> Architecture -> Motd -> Property (HasInfo + Debian)
resSystem suite arch motd = propertyList "Soleil RES system" $ props
  & proxySetup proxy
  & Apt.proxy proxy
  & standardSystemUnhardened suite arch motd (Group "grp-instrumentation")
  & Timezone.configured "Europe/Paris"
  & ntp "ntp.synchrotron-soleil.fr"
  & Apt.installed ["apt-transport-https"]
  & sources
  & Apt.unattendedUpgrades
  & packages (wm Gnome)
  & backports []
  & Apt.removed []
  & Apt.autoRemove
  & Apt.cacheCleaned
  & ldap "ldap://195.221.10.1"
  & (scriptProperty ["apt-file update"] `assume` MadeChange) `period` Daily
  & gdmConfig

  -- Configure firefox to work with a local jupyter notebook
  & firefoxProxy [ (FPref "network.proxy.backup.ftp" proxyIp)
                 , (FPref "network.proxy.backup.ftp_port" proxyPort)
                 , (FPref "network.proxy.backup.socks" proxyIp)
                 , (FPref "network.proxy.backup.socks_port" proxyPort)
                 , (FPref "network.proxy.backup.ssl" proxyIp)
                 , (FPref "network.proxy.backup.ssl_port" proxyPort)
                 , (FPref "network.proxy.ftp" proxyIp)
                 , (FPref "network.proxy.ftp_port" proxyPort)
                 , (FPref "network.proxy.http" proxyIp)
                 , (FPref "network.proxy.http_port" proxyPort)
                 , (FPref "network.proxy.share_proxy_settings" (FBool True))
                 , (FPref "network.proxy.socks" proxyIp)
                 , (FPref "network.proxy.socks_port" proxyPort)
                 , (FPref "network.proxy.ssl" proxyIp)
                 , (FPref "network.proxy.ssl_port" proxyPort)
                 , (FPref "network.proxy.type" (FInt 1))
                 ]

  & Sudo.enabledFor (User "picca")
  & redirectRoot "picca@synchrotron-soleil.fr" "smtp.synchrotron-soleil.fr" Satellite
  & mySetup (User "picca")
  & Soleil.withUserHomeHasContent ".ssh/config" (User "picca") sshConfig

  & Cron.runPropellor (Cron.Times "30 * * * *")
  where
    proxy = "http://195.221.0.35:8080"
    proxyIp = FString "195.221.0.35"
    proxyPort = FInt 8080

ord03037 :: Host
ord03037 = host "ORD03037.synchrotron-soleil.fr" $ props
  & proxySetup proxy
  & Apt.proxy proxy
  & standardSystem (Stable "stretch") X86_32 [ "Welcome to ORD03037!" ] (Group "grp-instrumentation")
  & Group.exists (Group "com-cristal") (Just 10003)
  & Group.exists (Group "com-diffabs") (Just 10006)
  & Group.exists (Group "com-mars") (Just 10012)
  & Group.exists (Group "com-sixs") (Just 10019)
  & User.hasGroup (User "picca") (Group "com-cristal")
  & User.hasGroup (User "picca") (Group "com-diffabs")
  & User.hasGroup (User "picca") (Group "com-mars")
  & User.hasGroup (User "picca") (Group "com-sixs")
  & sources
  & packages (wm Awesome)
  & backports []
  & Apt.autoRemove
  & redirectRoot "picca@synchrotron-soleil.fr" "smtp.synchrotron-soleil.fr" Smarthost
  & mounts DoMount [ MountConf "nfs" "ruche.synchrotron-soleil.fr:/cristal-soleil"  "/nfs/ruche-cristal/cristal-soleil" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/cristal-users"   "/nfs/ruche-cristal/cristal-users" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/diffabs-soleil"  "/nfs/ruche-diffabs/diffabs-soleil" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/diffabs-users"   "/nfs/ruche-diffabs/diffabs-users" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/mars-soleil"     "/nfs/ruche-mars/mars-soleil" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/mars-users"      "/nfs/ruche-mars/mars-users" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/sixs-soleil"     "/nfs/ruche-sixs/sixs-soleil" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/sixs-users"      "/nfs/ruche-sixs/sixs-users" rucheOpts
                   , MountConf "nfs" "ruche.synchrotron-soleil.fr:/instrumentation" "/nfs/instrumentation" rucheOpts
                   , MountConf "nfs" "filer-exp.synchrotron-soleil.fr:/FS_PTGRES/experiences/PICCA" "/nfs/filer/PICCA" mempty
                   , MountConf "nfs" "filer-exp.synchrotron-soleil.fr:/FS_PTGRES/experiences/Common" "/nfs/filer/Common" mempty
                   , MountConf "nfs" "pasiphae.synchrotron-soleil.fr:/FS_PTGRES/SOLEIL/Tempo2Mois" "/nfs/tempo2mois" mempty
                   , MountConf "nfs" "filer-exp.synchrotron-soleil.fr:/FS_TMPEXP/share-temp" "/nfs/share-temp" mempty
                   ]
  & hklDev
  -- & hklHs
  & Soleil.rra
  & sbuild X86_32 (User "picca") ord03037
  & Soleil.gpgUserConfig (GpgKeyId "0x5632906F4696E015") (User "picca")
  -- & debomatic "/srv/debomatic-i386" (User "debomatic") (Group "debomatic") "ord03037.synchrotron-soleil.fr" gpgKey repo
  -- & debomaticDputNg ord03037 "soleil"
  -- & File.ownerGroup vmpath (User "picca") (Group "instrumentation")
  -- & File.mode vmpath accessModes
  -- & mkVBox jupyter (vmpath </> "jupyter.img")
  & File.containsLine "/etc/hosts" "172.28.2.18 esmeralda.ipanema.cnrs.fr esmeralda"
  & Cron.runPropellor (Cron.Times "30 * * * *")
  where
    proxy = "http://195.221.0.35:8080"
    gpgKey = ""
    repo = "ord03037"
    vmpath = "/nfs/share-temp/picca/vm"

-- ord03244 :: Host
-- ord03244 = host "ord03244.synchrotron-soleil.fr" $ props
--   & homeSystem (Stable "buster") X86_64 [ "Welcome to ord03244" ]
--   & Apt.useLocalCacher
--   & (Sbuild.built Sbuild.UseCcache $ props
--       & osDebian (Stable "buster") X86_64
--       & Sbuild.useHostProxy ord03244
--       & Sbuild.update `period` Weekly (Just 1))
--   & (Sbuild.built Sbuild.UseCcache $ props
--       & osDebian Unstable X86_64
--       & Sbuild.useHostProxy ord03244
--       & Sbuild.update `period` Weekly (Just 1))
--   & Sbuild.userConfig (User "picca")
--   & Sbuild.usableBy (User "picca")
--   & Schroot.overlaysInTmpfs
--   & hklDev
--  -- & hklHs
--  -- & relSystem

grades_01 :: Host
grades_01 = host "grades-01.synchrotron-soleil.fr" $ props
  & resSystem (Stable "buster") X86_64 [ "Welcome on grades-01! (BEWARE no backup on this computer)" ]
  & Sudo.enabledFor (User "farhie")
  & doNotSuspend
  & Network.static' "enp68s0" (IPv4 "195.221.4.1/27") (Just (Network.Gateway (IPv4 "195.221.4.62"))) [("dns-nameservers", "195.221.2.96 195.221.2.97")
                                                                                                     , ("dns-search", "synchrotorn-soleil.fr")]
  -- & mountSoleil Sixs FsCache `version` (3 :: Int)
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian (Stable "buster") X86_64
      & Sbuild.useHostProxy grades_01
      & Sbuild.update `period` Weekly (Just 1))
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian Unstable X86_64
      & Sbuild.useHostProxy grades_01
      & Sbuild.update `period` Weekly (Just 1))
  & Sbuild.userConfig (User "picca")
  & Sbuild.usableBy (User "picca")
  & Schroot.overlaysInTmpfs

jupyter :: Host
jupyter = host "jupyter.synchrotron-soleil.fr" $ props
  & proxySetup proxy
  & Apt.proxy proxy
  & standardSystem (Stable "stretch") X86_64 [ "Welcome to jupyter!" ] (Group "grp-instrumentation")
  & Apt.installed [ "linux-image-amd64"
                  , "open-vm-tools"
                  , "ca-certificates"
                  , "locales"
                  ]
  & Grub.installed Grub.PC
  & Network.static' "ens32" (IPv4 "195.221.0.155")
  (Just (Network.Gateway (IPv4 "195.221.0.190")))
  [("netmask", "255.255.255.192")]
  -- temporary
  & User.hasInsecurePassword (User "root") "root"
  & Cron.runPropellor (Cron.Times "30 * * * *")
  & Soleil.mkVBox (panbox proxy) (Soleil.VirtualBoxFlat ("/" </> "root" </> "vm" </> "panbox-flat.vmdk")) 3000
  where
    proxy = "http://195.221.0.35:8080"

panbox :: String -> Host
panbox proxy = host "panbox.synchrotron-soleil.fr" $ props
  & standardSystem (Stable "buster") X86_64 [ "Welcome to panbox!" ] (Group "picca")
  & proxySetup proxy
  & Apt.proxy proxy
  & Apt.installed [ "linux-image-amd64" ]
  & Grub.installed Grub.PC
  & Apt.installed (wm Xfce4)
  & Apt.installed [ "bash-completion"
                  -- , "binoculars"
                  -- , "jupyter-notebook"
                  , "locales"
                  , "qemu-guest-agent"
                  , "network-manager"
                  -- , "xterm"
                  ]
  & User "root" `User.hasInsecurePassword` "root"
  & User.accountFor (User "panbox")
  & User "panbox" `User.hasInsecurePassword` "panbox"
  & ldap "ldap://195.221.10.1"
  & mounts DoNotMount [ MountConf "nfs" "ruche.synchrotron-soleil.fr:/cristal-soleil"  "/nfs/ruche/cristal-soleil" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/cristal-users"   "/nfs/ruche/cristal-users" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/diffabs-soleil"  "/nfs/ruche/diffabs-soleil" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/diffabs-users"   "/nfs/ruche/diffabs-users" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/mars-soleil"     "/nfs/ruche/mars-soleil" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/mars-users"      "/nfs/ruche/mars-users" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/sixs-soleil"     "/nfs/ruche/sixs-soleil" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/sixs-users"      "/nfs/ruche/sixs-users" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/experiences"     "/nfs/ruche/experiences" rucheOpts
                      , MountConf "nfs" "ruche.synchrotron-soleil.fr:/instrumentation" "/nfs/ruche/instrumentation" rucheOpts
                      , MountConf "nfs" "filer-exp.synchrotron-soleil.fr:/FS_TMPEXP/share-temp" "/nfs/share-temp" mempty
                      ]

-- | Soleil Computer REL

relSystem :: DebianSuite -> Architecture -> Motd -> Property (HasInfo + Debian)
relSystem suite arch motd = propertyList "Soleil REL system" $ props
  & proxySetup proxy
  & Apt.proxy proxy
  & standardSystemUnhardened suite arch motd (Group "grp-instrumentation")
  & Ssh.setSshdConfigBool "StreamLocalBindUnlink" True  -- for nitrokey
  & Timezone.configured "Europe/Paris"
  & ntp "ntp.exp.synchrotron-soleil.fr"
  & Apt.installed ["apt-transport-https"]
  & sources
  & Apt.unattendedUpgrades
  & packages (wm Gnome)
  & backports []
  & Apt.removed packagesToRemove
  & Apt.autoRemove
  & Apt.cacheCleaned
  & ldap "ldap://ldap.exp.synchrotron-soleil.fr"
  & (scriptProperty ["apt-file update"] `assume` MadeChange) `period` Daily
  & gdmConfig
  & doNotSuspend

  -- Configure firefox to work with a local jupyter notebook
  & firefoxProxy [ (FPref "network.proxy.backup.ftp" proxyIp)
                 , (FPref "network.proxy.backup.ftp_port" proxyPort)
                 , (FPref "network.proxy.backup.socks" proxyIp)
                 , (FPref "network.proxy.backup.socks_port" proxyPort)
                 , (FPref "network.proxy.backup.ssl" proxyIp)
                 , (FPref "network.proxy.backup.ssl_port" proxyPort)
                 , (FPref "network.proxy.ftp" proxyIp)
                 , (FPref "network.proxy.ftp_port" proxyPort)
                 , (FPref "network.proxy.http" proxyIp)
                 , (FPref "network.proxy.http_port" proxyPort)
                 , (FPref "network.proxy.share_proxy_settings" (FBool True))
                 , (FPref "network.proxy.socks" proxyIp)
                 , (FPref "network.proxy.socks_port" proxyPort)
                 , (FPref "network.proxy.ssl" proxyIp)
                 , (FPref "network.proxy.ssl_port" proxyPort)
                 , (FPref "network.proxy.type" (FInt 1))
                 ]

  & Sudo.enabledFor (User "picca")
  & redirectRoot "picca@synchrotron-soleil.fr" "smtp.synchrotron-soleil.fr" Satellite
  & mySetup (User "picca")
  & Soleil.withUserHomeHasContent ".ssh/config" (User "picca") sshConfig

  & Sudo.enabledFor (User "farhie")

  & Cron.runPropellor (Cron.Times "30 * * * *")
  where
    proxy = "http://195.221.10.6:8080"
    proxyIp = FString "195.221.10.6"
    proxyPort = FInt 8080


cristal4 :: Host
cristal4 = host "cristal4.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on cristal4! (BEWARE no backup on this computer)" ]
  & Ssh.passwordAuthentication True
  & Ssh.authorizedKey' (User "elkaim") (Group "cristal") sshKeyPubElkaim
  & Ssh.authorizedKey' (User "berenguer") (Group "cristal") sshKeyPubBerenguer
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  & installVideo NVidiaVideo
  & mountSoleil Cristal NoFsCache `version` (2 :: Int)
  & sbuild X86_64 (User "picca") cristal4
  & gprof2dot
  & kkcalc
  & bcdi (Just "devel")
  & User.accountFor (User "lara")
  & User.accountFor (User "pynx")
  & hklDev
  -- & hklHs
  & binocularsLatest
  -- & pyFAI

diffabs6 :: Host
diffabs6 = host "diffabs6.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on diffabs6!" ]
  & User.accountFor (User "pyfai")
  -- & Soleil.installOpenCL (Soleil.AMDGPUPROOpenCL "amdgpu-pro-18.20-579836.tar.xz")
  -- & Soleil.installOpenCL Soleil.MesaOpenCL
  & installVideo DefaultVideo
  & mountSoleil Diffabs NoFsCache `version` (2 :: Int)
  & mounts DoMount [ MountConf "ext4" "/dev/sdb1" "/srv" mempty ]
  -- & Soleil.debomatic incoming (User "debomatic") (Group "debomatic") "diffabs6.exp.synchrotron-soleil.fr" "1D7C5C186F43C731" sshKeyPub repo
  -- & Soleil.debomaticDputNg diffabs6 "soleil"
  -- & Apt.setSourcesListD [ "deb [trusted=yes] file:" ++ (incoming ++ "/stable") ++" stable main contrib non-free" ] repo
  & Ssh.authorizedKey' (User "reguer") (Group "grp-com-diffabs") sshKeyPubReguerSrv4
  & Ssh.authorizedKey' (User "com-diffabs") (Group "grp-com-diffabs") sshKeyPubComDiffabsSrv4
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian (Stable "buster") X86_64
      & Sbuild.useHostProxy diffabs6)
      & Sbuild.update `period` Weekly (Just 1)
  & Sbuild.userConfig (User "mariea")
  & Sbuild.usableBy (User "mariea")
  & Schroot.overlaysInTmpfs
  -- & hklDev
  -- & autoprocessingDev
  -- & gprof2dot
  -- & hklHs
  where
    incoming = "/srv/debomatic-amd64"
    repo = "diffabs6"
  -- & ufo

hermes17 :: Host
hermes17 = host "hermes17.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on hermes17! (BEWARE no backup on this computer)" ]
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  & installVideo NVidiaVideo
  -- raid
  -- mdadm --create /dev/md0 --level=0 --raid-devices=6 /dev/sdc /dev/sdd /dev/sde /dev/sdf /dev/sdg /dev/sdh
  -- mkfs.ext4 /dev/md0
  -- fstab /dev/md0 /home ext3 noatime,rw 0 0
  -- mdadm --detail --scan /dev/md0
  -- ARRAY /dev/md0 metadata=1.2 name=hermes17:0 UUID=af6cbf5c:157699ef:a90c815f:f5e09b64
  -- blkid /dev/md0
  -- /dev/md0: UUID="aa9a912a-373b-4d06-b9fe-3dd99f304ae9" TYPE="ext4" ???
  & cachefs (MountConf "ext4" "UUID=\"aa9a912a-373b-4d06-b9fe-3dd99f304ae9\""  "/var/cache/fscache" (Mount.MountOpts ["noatime", "rw"]))
  & mountSoleil Hermes FsCache `version` (3 :: Int)
  -- need cuda 10 for the graphic card.
  -- & Apt.installed ["nvidia-cuda-toolkit"]
  -- & scikitCuda
  & pynx (Just "hermes")
  -- & Soleil.teamviewer


mars2 :: Host
mars2 = host "mars2.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on mars2!" ]
  -- & Soleil.installOpenCL (Soleil.AMDGPUPROOpenCL "amdgpu-pro-18.20-579836.tar.xz")
  -- & Soleil.installOpenCL Soleil.MesaOpenCL
  & installVideo DefaultVideo
  & mountSoleil Mars NoFsCache `version` (2 :: Int)
  & sbuild X86_64 (User "picca") mars2
  & Schroot.overlaysInTmpfs
  & gprof2dot
  & hklDev
  -- & hklHs

nanoscopium8 :: Host
nanoscopium8 = host "nanoscopium8.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on nanoscopium8! (BEWARE no backup on this computer)" ]
  & Ssh.authorizedKey' (User "com-nanoscopium") (Group "grp-com-nanoscopium") sshKeyPubKadda
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  & installVideo NVidiaVideo
  & Apt.installed ["nvidia-cuda-toolkit"]
  & mountSoleil Nanoscopium NoFsCache `version` (2 :: Int)
  & pynx (Just "devel")
  & pyhst2
  & User.accountFor (User "serge")
  & Soleil.gpgUserConfig (GpgKeyId "0xF8EFC246D17B29CC") (User "serge")

ode15 :: Host
ode15 = host "ode15.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "jessie") X86_64 [ "Welcome on !ode15 (BEWARE no backup on this computer)" ]
  & installVideo DefaultVideo
  & mountSoleil Ode NoFsCache `version` (1 :: Int)
  -- gotthard detector
  & Apt.installed [ "libqt4-dev"
                  , "libqwt-dev"
                  , "libqwtplot3d-qt4-dev"
                  , "root-system"
                  ]
  & Soleil.fetch' "https://www.psi.ch/detectors/UsersSupportEN/slsDetectorsPackage_v2.0.3.tgz" "/root/slsDetectorsPackage_v2.0.3.tgz"

proxima1_23 :: Host
proxima1_23 = host "proxima1-23.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on proxima1-23! (BEWARE no backup on this computer)" ]
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  & mounts DoMount [ MountConf "nfs" "195.221.8.78:/data1-1"              "/data1-1" opts1
                   , MountConf "nfs" "195.221.8.78:/data1-2"              "/data1-2" opts1
                   , MountConf "nfs" "195.221.8.78:/data1-3"              "/data1-3" opts1
                   , MountConf "nfs" "195.221.8.82:/data4"                "/data4"   opts1
                   , MountConf "nfs" "195.221.8.92:/proxima1-soleil"      "/nfs/ruche/proxima1-soleil" opts2
                   , MountConf "nfs" "195.221.8.92:/proxima1-users"       "/nfs/ruche/proxima1-users"  opts2
                   , MountConf "nfs" "195.221.10.9:/FS_SHAREDEV"          "/nfs/ruche/share-dev"       opts2
                   , MountConf "nfs" "195.221.10.9:/FS_TMPEXP/share-temp" "/nfs/ruche/share-temp"      opts2
                   ]
  & hdf5plugin
  & bcdi (Just "master")
  & installVideo NVidiaVideo
  & Apt.installed [ "python-pycuda", "python3-pycuda" ]
  & Grub.cmdline_Linux_default "vsyscall=emulate" -- CentOS6 binaries #889965
  & autoprocessingDev
  where
    opts1 = Mount.MountOpts [ "noauto", "x-systemd.automount", "x-systemd.device-timeout=10", "timeo=14", "x-systemd.idle-timeout=1min" ]
    opts2 = Mount.MountOpts [ "noauto", "x-systemd.automount", "x-systemd.device-timeout=10", "timeo=14", "x-systemd.idle-timeout=1min", "rw", "intr", "hard", "acdirmin=1", "vers=3"]

proxima1_26 :: Host
proxima1_26 = host "proxima1-26.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on proxima1-26! (BEWARE no backup on this computer)" ]
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  & pynx (Just "master")
  & installVideo NVidiaVideo
  & Apt.installed [ "autofs" ]
  & Apt.installed [ "python-pycuda", "python3-pycuda" ]
  & Grub.cmdline_Linux_default "vsyscall=emulate" -- CentOS6 binaries #889965
  & autoprocessingDev
  & mounts DoMount [ MountConf "xfs" "/dev/md126p1"                       "/datagpu1" mempty
                   , MountConf "xfs" "/dev/sdb1"                          "/datagpu2" mempty
                   , MountConf "nfs" "195.221.8.78:/data1-1"              "/data1-1" opts1
                   , MountConf "nfs" "195.221.8.78:/data1-2"              "/data1-2" opts1
                   , MountConf "nfs" "195.221.8.78:/data1-3"              "/data1-3" opts1
                   , MountConf "nfs" "195.221.8.82:/data4"                "/data4"   opts1
                   , MountConf "nfs" "195.221.8.92:/proxima1-soleil"      "/nfs/ruche/proxima1-soleil" opts2
                   , MountConf "nfs" "195.221.8.92:/proxima1-users"       "/nfs/ruche/proxima1-users"  opts2
                   , MountConf "nfs" "195.221.10.9:/FS_SHAREDEV"          "/nfs/ruche/share-dev"       opts2
                   , MountConf "nfs" "195.221.10.9:/FS_TMPEXP/share-temp" "/nfs/ruche/share-temp"      opts2
                   ]
  where
    opts1 = Mount.MountOpts [ "noauto", "x-systemd.automount", "x-systemd.device-timeout=10", "timeo=14", "x-systemd.idle-timeout=1min" ]
    opts2 = Mount.MountOpts [ "noauto", "x-systemd.automount", "x-systemd.device-timeout=10", "timeo=14", "x-systemd.idle-timeout=1min", "rw", "intr", "hard", "acdirmin=1", "vers=3"]

rock1 :: Host
rock1 = host  "rock1.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "bullseye") X86_64 [ "Welcome on !rock1 (BEWARE no backup on this computer)" ]
  & mountSoleil Rock NoFsCache `version` (1 :: Int)
  & Soleil.installOpenCL Soleil.NVidiaOpenCL

rock2 :: Host
rock2 = host "rock2.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "jessie") X86_64 [ "Welcome on !rock2 (BEWARE no backup on this computer)" ]
  & Soleil.installOpenCL Soleil.BeignetOpenCL
  & installVideo DefaultVideo
  & mountSoleil Rock NoFsCache `version` (1 :: Int)
  & Sudo.enabledFor (User "com-rock")

panboxDocker :: Docker.Container
panboxDocker = Docker.container "panbox-docker" (Docker.latestImage "debian") $ props
               & Docker.publish "80:80"
               & Docker.environment ("http_proxy", "http://195.221.7.0:8080")
               & Docker.environment ("https_proxy", "http://195.221.7.0:8080")

re_grades_01 :: Host
re_grades_01 = host "re-grades-01.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "bullseye") X86_64 [ "Welcome on re-grades-01! (BEWARE no backup on this computer)" ]
  & Sudo.enabledFor (User "farhie")
  & Sudo.enabledFor (User "picca")
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  -- raid
  -- mdadm --create /dev/md0 --level=0 --raid-devices=3 /dev/sda /dev/sdb /dev/sdc
  -- mkfs.ext4 /dev/md0
  -- mdadm --detail --scan /dev/md0 >> /etc/mdadm/mdadm.conf
  -- blkid /dev/md0
  -- /dev/md0: UUID="8cc7bd77-9c77-428f-a935-3e07283cf301" BLOCK_SIZE="4096" TYPE="ext4"
  -- & (File.hasContent "/etc/mdadm/mdadm.conf" [ "# DO NOT EDIT, MANAGED VIA PROPELLOR"
  --                                            , "# mdadm.conf"
  --                                            , "#"
  --                                            , "# !NB! Run update-initramfs -u after updating this file."
  --                                            , "# !NB! This will ensure that initramfs has an uptodate copy."
  --                                            , "#"
  --                                            , "# Please refer to mdadm.conf(5) for information about this file."
  --                                            , "#"
  --                                            , ""
  --                                            , "# by default (built-in), scan all partitions (/proc/partitions) and all"
  --                                            , "# containers for MD superblocks. alternatively, specify devices to scan, using"
  --                                            , "# wildcards if desired."
  --                                            , "#DEVICE partitions containers"
  --                                            , ""
  --                                            , "# automatically tag new arrays as belonging to the local system"
  --                                            , "HOMEHOST <system>"
  --                                            , ""
  --                                            , "# instruct the monitoring daemon where to send mail alerts"
  --                                            , "MAILADDR root"
  --                                            , ""
  --                                            , "ARRAY /dev/md0 metadata=1.2 name=re-grades-01:0 UUID=9bf59dbc:451b39c8:a9b6fc70:516341d7"
  --                                            ]
  --     `onChange` (cmdProperty "update-initramfs" ["-u"] `assume` MadeChange))
  & Soleil.fstab [ Soleil.FsCmt "# /etc/fstab: static file system information."
                 , Soleil.FsCmt "#"
                 , Soleil.FsCmt "# Use 'blkid' to print the universally unique identifier for a"
                 , Soleil.FsCmt "# device; this may be used with UUID= as a more robust way to name devices"
                 , Soleil.FsCmt "# that works even if disks are added and removed. See fstab(5)."
                 , Soleil.FsCmt "#"
                 , Soleil.FsCmt "# systemd generates mount units based on this file, see systemd.mount(5)."
                 , Soleil.FsCmt "# Please run 'systemctl daemon-reload' after making changes here."
                 , Soleil.FsCmt "#"
                 , Soleil.FsCmt "# <file system> <mount point>   <type>  <options>       <dump>  <pass>"
                 , Soleil.FsCmt "# / was on /dev/nvme0n1p1 during installation"
                 , Soleil.FsMnt "UUID=4be70abe-d4e8-460c-9161-ec3217d4a755 /               ext4    errors=remount-ro 0       1"
                 , Soleil.FsCmt "# /home was on /dev/md0 during installation"
                 , Soleil.FsMnt "UUID=ef945d5f-0ace-44c9-9211-6a073be888e0 /home           ext4    defaults        0       2"
                 , Soleil.FsCmt "#"
                 , Soleil.FsCmt "# SOLEIL/Ruche mounts MUST be static. Autofs fails (but we keep it to get a list of all mounts in /net)"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/ailes-soleil      /nfs/ruche/ailes-soleil        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/ailes-users       /nfs/ruche/ailes-users         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/anatomix-soleil   /nfs/ruche/anatomix-soleil     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/anatomix-spool    /nfs/ruche/anatomix-spool      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/anatomix-users    /nfs/ruche/anatomix-users      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/antares-soleil    /nfs/ruche/antares-soleil      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/antares-users     /nfs/ruche/antares-users       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/archives          /nfs/ruche/archives            nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/biologie          /nfs/ruche/biologie            nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/biophys           /nfs/ruche/biophys             nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/cassiopee-soleil  /nfs/ruche/cassiopee-soleil    nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/cassiopee-users   /nfs/ruche/cassiopee-users     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/chimie            /nfs/ruche/chimie              nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/contacq           /nfs/ruche/contacq             nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/coxinel-soleil    /nfs/ruche/coxinel-soleil      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/coxinel-users     /nfs/ruche/coxinel-users       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/cristal-soleil    /nfs/ruche/cristal-soleil      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/cristal-users     /nfs/ruche/cristal-users       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/deimos-soleil     /nfs/ruche/deimos-soleil       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/deimos-users      /nfs/ruche/deimos-users        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/desirs-soleil     /nfs/ruche/desirs-soleil       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/desirs-users      /nfs/ruche/desirs-users        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/detecteurs        /nfs/ruche/detecteurs          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/diffabs-soleil    /nfs/ruche/diffabs-soleil      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/diffabs-users     /nfs/ruche/diffabs-users       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/direction         /nfs/ruche/direction           nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/dir-exp           /nfs/ruche/dir-exp             nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/disco-soleil      /nfs/ruche/disco-soleil        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/disco-users       /nfs/ruche/disco-users         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/experiences       /nfs/ruche/experiences         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/flyscan-soleil    /nfs/ruche/flyscan-soleil      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/galaxies-soleil   /nfs/ruche/galaxies-soleil     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/galaxies-users    /nfs/ruche/galaxies-users      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/hermes-soleil     /nfs/ruche/hermes-soleil       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/hermes-users      /nfs/ruche/hermes-users        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/ica               /nfs/ruche/ica                 nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/informatique      /nfs/ruche/informatique        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/instrumentation   /nfs/ruche/instrumentation     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/ipanema           /nfs/ruche/ipanema             nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/lucia-soleil      /nfs/ruche/lucia-soleil        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/lucia-users       /nfs/ruche/lucia-users         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/mars-soleil       /nfs/ruche/mars-soleil         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/mars-users        /nfs/ruche/mars-users          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/metrologie-soleil /nfs/ruche/metrologie-soleil   nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/metrologie-users  /nfs/ruche/metrologie-users    nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/microflu          /nfs/ruche/microflu            nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/nanoprobe         /nfs/ruche/nanoprobe           nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/nanoscopium-soleil /nfs/ruche/nanoscopium-soleil nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/nanoscopium-users /nfs/ruche/nanoscopium-users   nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/ode-soleil        /nfs/ruche/ode-soleil          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/ode-users         /nfs/ruche/ode-users           nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/pleiades-soleil   /nfs/ruche/pleiades-soleil     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/pleiades-users    /nfs/ruche/pleiades-users      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/proxima1-soleil   /nfs/ruche/proxima1-soleil     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/proxima1-users    /nfs/ruche/proxima1-users      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/proxima2a-soleil  /nfs/ruche/proxima2a-soleil    nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/proxima2a-users   /nfs/ruche/proxima2a-users     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/psiche-soleil     /nfs/ruche/psiche-soleil       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/psiche-users      /nfs/ruche/psiche-users        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/puma-soleil       /nfs/ruche/puma-soleil         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/puma-users        /nfs/ruche/puma-users          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/rcm-data          /nfs/ruche/rcm-data            nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/rcm-pub           /nfs/ruche/rcm-pub             nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/rock-soleil       /nfs/ruche/rock-soleil         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/rock-users        /nfs/ruche/rock-users          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/samba-soleil      /nfs/ruche/samba-soleil        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/samba-users       /nfs/ruche/samba-users         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sextants-soleil   /nfs/ruche/sextants-soleil     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sextants-users    /nfs/ruche/sextants-users      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/share-temp        /nfs/ruche/share-temp          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sirius-soleil     /nfs/ruche/sirius-soleil       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sirius-users      /nfs/ruche/sirius-users        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sixs-soleil       /nfs/ruche/sixs-soleil         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sixs-users        /nfs/ruche/sixs-users          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/slicing-soleil    /nfs/ruche/slicing-soleil      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/slicing-users     /nfs/ruche/slicing-users       nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/smis-soleil       /nfs/ruche/smis-soleil         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/smis-users        /nfs/ruche/smis-users          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sources           /nfs/ruche/sources             nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/sr                /nfs/ruche/sr                  nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/surfaces-soleil   /nfs/ruche/surfaces-soleil     nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/surfaces-users    /nfs/ruche/surfaces-users      nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/swing-soleil      /nfs/ruche/swing-soleil        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/swing-users       /nfs/ruche/swing-users         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/syslogs           /nfs/ruche/syslogs             nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/techniques        /nfs/ruche/techniques          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/tempo-soleil      /nfs/ruche/tempo-soleil        nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 , Soleil.FsMnt "ruche.exp.synchrotron-soleil.fr:/tempo-users       /nfs/ruche/tempo-users         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount,user 0 2"
                 ]
  & Grub.cmdline_Linux_default "amd_iommu=pt"
  & File.hasContent "/etc/modprobe.d/qemu-web-desktop.conf" ["options vfio-pci ids=10de:1eb8 disable_vga=1"]
  & File.hasContent "/etc/modules-load.d/01-qemu-web-desktop.conf" ["vfio-pci"]


sextants6 :: Host
sextants6 = host "sextants6.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on sextantTmp! (BEWARE no backup on this computer)" ]
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  & installVideo NVidiaVideo
  & Apt.installed ["nvidia-cuda-toolkit"]
  -- & mountSoleil Sextants NoFsCache `version` (2 :: Int)
  & pynx (Just "devel")

sixs3 :: Host -- May 21st, 2019
sixs3 = host "sixs3.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "buster") X86_64 [ "Welcome on six3 (BEWARE no backup on this computer)" ]
  & Apt.installed [ "resolvconf"
                  , "python-guiqwt"  -- pour yves durant la quarantaine.
                  ]
  & Network.static' "ens1f0" (IPv4 "172.19.23.24/24") Nothing [ ("broadcast", "172.19.23.255")
                                                              , ("dns-nameservers", "172.19.23.1")
                                                              , ("dns-search", "sixs.rcl")]
  & Network.static' "ens1f1" (IPv4 "195.221.9.195/27") (Just (Network.Gateway (IPv4 "195.221.9.222"))) [("dns-nameservers", "195.221.10.1 195.221.10.2")
                                                                                                       , ("dns-search", "exp.synchrotorn-soleil.fr")]
  & installVideo DefaultVideo
  & cachefs (MountConf "xfs" "UUID=\"f1dab658-95be-4763-b6a6-5d81882cace0\""  "/var/cache/fscache" mempty)
  & mountSoleil Sixs FsCache `version` (3 :: Int)
  -- & Soleil.archttp
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian (Stable "buster") X86_64
      & Sbuild.update `period` Weekly (Just 1)
      & Sbuild.useHostProxy sixs3)
  & (Sbuild.built Sbuild.UseCcache $ props
      & osDebian Unstable X86_64
      & Sbuild.update `period` Weekly (Just 1)
      & Sbuild.useHostProxy sixs3)
  & Sbuild.userConfig (User "picca")
  & Sbuild.usableBy (User "picca")
  & Ssh.authorizedKey' (User "com-sixs") (Group "grp-com-sixs") sshKeyPubComSixs
  & Ssh.authorizedKey' (User "com-sixs") (Group "grp-com-sixs") sshKeyPubComSixsSrv4
  & Ssh.passwordAuthentication True
  & Soleil.installOpenCL Soleil.NVidiaOpenCL
  & Soleil.dhtChroot (User "picca") X86_64
  -- & Soleil.mkVBox (panbox "http://195.221.10.7:8080") (DiskImage.RawDiskImage panboxName) 3000
  -- & File.ownerGroup panboxName (User "picca") (Group "grp-instrumentation")
  -- & "/nfs/share-temp/panbox-rel.img" `Soleil.isCopyOf` panboxName
  -- & Docker.docked panboxDocker
  & hklDev
  & xrayutilities_bpo
  & bcdi (Just "devel")
  -- & xsocs
  -- & hklHs
  where
    panboxName = "/root/vm/panbox-rel.img"

sixs7 :: Host
sixs7 = host "sixs7.exp.synchrotron-soleil.fr" $ props
  & relSystem (Stable "bullseye") X86_64 [ "Welcome on !six7 (BEWARE no backup on this computer)" ]
  & Soleil.installOpenCL Soleil.BeignetOpenCL
  & installVideo DefaultVideo
  & mountSoleil Sixs NoFsCache `version` (2 :: Int)
  & hklDev
  -- & hklHs

-- | Soleil Computer RCL

rclSystem :: DebianSuite -> Architecture -> Motd -> Property (HasInfo + Debian)
rclSystem suite arch motd = propertyList "Soleil RCL system" $ props
  & proxySetup proxy
  & Apt.proxy proxy
  & standardSystemUnhardened suite arch motd (Group "grp-instrumentation")
  & Timezone.configured "Europe/Paris"
  & ntp "ntp"
  & Apt.installed ["apt-transport-https"]
  & sources
  & Apt.unattendedUpgrades
  & packages []
  & backports [ "linux-image-amd64"
              , "linux-headers-amd64"
              , "firmware-linux-nonfree"
              ] -- all computer on exp are amd64
  & Apt.autoRemove
  & Apt.cacheCleaned
  & Apt.reConfigure "libnss-ldapd" [ ("libnss-ldapd/nsswitch", "multiselect", "group, passwd, shadow") ] `requires`  Apt.installed ["libnss-ldapd"]
  & File.containsBlock "/etc/nslcd.conf" [ "base dc=EXP" ] `onChange` Service.restarted "nslcd"
  & Apt.installed ["libpam-ldapd"]
  & File.containsBlock  "/etc/ldap/ldap.conf" [ "URI ldap://195.221.10.1"  -- oxygen
                                              , "BASE dc=EXP"
                                              ]
  & File.hasContent "/usr/share/pam-configs/mkhomedir" [ "Name: Create home directory during login"
                                                       , "Default: yes"
                                                       , "Priority: 900"
                                                       , "Session-Type: Additional"
                                                       , "Session:"
                                                       , "        required        pam_mkhomedir.so umask=0022 skel=/etc/skel"
                                                       ]
  `onChange` (scriptProperty ["pam-auth-update --package"] `assume` MadeChange)
  -- & (scriptProperty ["apt-file update"] `assume` MadeChange) `period` Daily
  & File.containsBlock "/etc/gdm3/greeter.dconf-defaults" [ "disable-user-list=true" ] -- need to deal with the restart


  -- Configure firefox to work with a local jupyter notebook
  & firefoxProxy [ (FPref "network.proxy.backup.ftp" proxyIp)
                 , (FPref "network.proxy.backup.ftp_port" proxyPort)
                 , (FPref "network.proxy.backup.socks" proxyIp)
                 , (FPref "network.proxy.backup.socks_port" proxyPort)
                 , (FPref "network.proxy.backup.ssl" proxyIp)
                 , (FPref "network.proxy.backup.ssl_port" proxyPort)
                 , (FPref "network.proxy.ftp" proxyIp)
                 , (FPref "network.proxy.ftp_port" proxyPort)
                 , (FPref "network.proxy.http" proxyIp)
                 , (FPref "network.proxy.http_port" proxyPort)
                 , (FPref "network.proxy.share_proxy_settings" (FBool True))
                 , (FPref "network.proxy.socks" proxyIp)
                 , (FPref "network.proxy.socks_port" proxyPort)
                 , (FPref "network.proxy.ssl" proxyIp)
                 , (FPref "network.proxy.ssl_port" proxyPort)
                 , (FPref "network.proxy.type" (FInt 1))
                 ]

  -- & sbuild arch (User "picca")
  -- & redirectRoot "picca@synchrotron-soleil.fr" "smtp.synchrotron-soleil.fr" Satellite
  & Soleil.gpgUserConfig (GpgKeyId "0x5632906F4696E015") (User "picca")
  -- & Cron.runPropellor (Cron.Times "30 * * * *")
  where
    proxy = "http://localhost:8080"
    proxyIp = FString "localhost"
    proxyPort = FInt 8080

-- ssh -N -f process1 -L8080:195.221.10.7:8080
internet :: User -> Property DebianLike
internet user@(User u) =  propertyList "Setup internet via ssh tunnels" $ props
  & config
  & "/etc/systemd/system/secure-tunnel.service" `ConfFile.iniFileContains` template
  & Systemd.running "secure-tunnel"
    where
      l :: [String]
      l = [ "Host process1"
          , "Hostname process1"
          , "User root"
          , "ProxyCommand ssh 172.19.11.24 nc -w 900 %h %p"
          ]

      config :: Property DebianLike
      config = property' (u ++ " config ssh") $ \w -> do
		f <- liftIO $ Ssh.dotFile "config" user
                ensureProperty w $ Ssh.modAuthorizedKey f user $
	                       f `File.containsLines` l
				`requires` File.dirExists (takeDirectory f)

      template = [ ("Unit",  [ ("Description", "Setup a secure tunnel to %I" )
                             , ("After", "network.target")
                             ])
                 , ("Service", [ ("Type", "forking")
                               -- ("User", "elchapo")
                               -- , ("Environment" , "\"LOCAL_ADDR=localhost\"")
                               -- , ("EnvironmentFile", "/etc/default/secure-tunnel@%i")
                               -- , ("ExecStart", "/usr/bin/ssh -NT -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -L ${LOCAL_ADDR}:${LOCAL_PORT}:localhost:${REMOTE_PORT} ${TARGET}")
                               , ("ExecStart", "/usr/bin/ssh -N -f process1 -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -L 8080:195.221.10.7:8080")
                               -- , "# Restart every >2 seconds to avoid StartLimitInterval failure"
                               , ("RestartSec", "5")
                               , ("Restart", "always")
                               ])
                 , ("Install", [ ("WantedBy", "multi-user.target") ])
                 ]


collect1 :: Host
collect1 = host "collect1.proxima1.rcl" $ props
  & proxySetup proxy
  & Apt.proxy proxy
  -- remove useless problematic interface from networking setup
  ! "/etc/network/interfaces.d/setup" `File.containsBlock` [ "auto eth0"
                                                           , "iface eth0 inet dhcp"
                                                           ]
  & Network.static' "enp4s0f0" (IPv4 "172.19.11.25") (Just (Network.Gateway (IPv4 "172.19.11.254"))) [ ("netmask", "255.255.255.0")
                                                                                                     , ("dns-nameservers", "172.19.11.1")
                                                                                                     , ("dns-search", "proxima1.rcl")]
  & internet (User "root")
  & rclSystem (Stable "stretch") X86_64 [ "Welcome on !collect1 (BEWARE no backup on this computer)" ]
  & Apt.installed (wm Xfce4)
  & mounts DoMount [ MountConf "nfs" "195.221.8.92:/proxima1-soleil"  "/nfs/ruche-proxima1/proxima1-soleil" rucheOpts
                   , MountConf "nfs" "195.221.8.92:/proxima1-users"   "/nfs/ruche-proxima1/proxima1-users" rucheOpts
                   , MountConf "nfs" "195.221.10.9:/FS_TMPEXP/share-temp" "/nfs/ruche-proxima1/share-temp" mempty
                   , MountConf "nfs" "195.221.10.9:/FS_SHAREDEV" "/nfs/ruche-proxima1/share-dev" mempty
                   ]
  -- & User.accountFor (User "elchapo")
  & sbuild X86_64 (User "picca") collect1
  where
    proxy = "localhost:8080"

-- | Standard system

type Motd = [String]

-- This is my standard system setup.

standardSystem :: DebianSuite -> Architecture -> Motd -> Group -> Property (HasInfo + Debian)
standardSystem suite arch motd group = propertyList "standard system" $ props
  & standardSystemUnhardened suite arch motd group
  & Ssh.noPasswords

standardSystemUnhardened :: DebianSuite -> Architecture -> Motd -> Group -> Property (HasInfo + Debian)
standardSystemUnhardened suite arch motd group = propertyList "standard system unhardened" $ props
  & bootstrapWith OSOnly
  & osDebian suite arch
  & Hostname.sane
  & Hostname.searchDomain
  & File.hasContent "/etc/motd" ("":motd++[""])
  & Apt.stdSourcesList `onChange` Apt.upgrade
  & Apt.installed ["etckeeper"]
  & Apt.installed ["ssh", "mosh", "git"]
  & PropellorRepo.hasOriginUrl "https://salsa.debian.org/picca/propellor.git"
  & Ssh.passwordAuthentication True
  & Ssh.authorizedKey (User "root") sshKeyPubPicca
  & Ssh.authorizedKey' (User "picca") group sshKeyPubPicca

sshKeyPubPicca :: String
sshKeyPubPicca = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDGkFpSsCIGpAJtsH4TWHCatHMkdGMS/PTG2M/7xeWz6Syw/JUrZPc/5bRC9H5+bikrhotZOidC+lafzGFHGmHzpq7+rXrd5Np3uVHH6U+Y0O7mUeU0CVhCpkIr2ggk4Bw7K79/d6fsPXZi2h+JAZ9cBaI6ob5K6e70Ljj3REZRh7LXBVIAd1hmMPEESb5xll1MHvB/7Qn6r6uupcOY/1pC/LH+ZPUaqvwXGrSltFjJoeFEW8H05uYkuZta5vBG/owdLjRt6v7h3tnINsMV4S0uKNQNz6022xAptn1FY1WQ0F1y738hTNoikITty//MB3HW3uQEpw4sXN7tEGqQtHrbMkPfcwb+KMISXYlHPaBt9ik4fWnt55U1IzXr5s/ErT6/ZCG2iPfnffuHnCVMujrUu+KcnHtF7Ux50N1QxR7+EiT6WxRDW3S6Vz0MQ6jTZdy/YryKYZtGnriFb2RwR7u9Y7Df+VYfj4nKrnF3JQF9yipBLcUhpliNvByvoh7eTE8iWuVlp3GkdHotEq4okH88TtUG5DBbddGHoGpxnzi8R4sn+YvFTybyw0whKgMQh0ueJ26j326AgujBDlvL3Hf6Satz/EDmwjStWGSwWQAcy+W+gfNAuRfHpyYHKDGPIJLzMfuf0vx0KLL0C55x7I4cGqOIT22RXLhhf9NFHNDi4Q== cardno:000500003084"

sshKeyPubSerge :: String
sshKeyPubSerge = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7/OvCzrLaqwEoDvc2pRNASGEf5ZwCSPWxiLSSHXT2I/mPlmn0bJUtDXCUoZXMYget4chd5ojWt2UJeDW6wO37yta6KDzA7PKxJ4d0hLjDAIkaUTDQHaA4ctSAvCdZxv549Ky/dp7kEPWbgNdWjH6nOiDbG+1I2t2PtXmwhmP0hvBm2NGFDkVnCJASmr2tzVi+p4ZHEHCr/tH395a78nqXSofRdFFNp4bs09OTafe/HA7doA2uhjKtwo6eRKIZwr9CEgAF+ES/JtwSdo5HtRH7gbwR5Eg+b3pnzW3+qQ5TD9gt4626SOP4eL89ErSomtuAs5RxjfmMQh2r6lDNVIY+ZKJLV8NHjwxyI3Y7X2/uNpTSiHNA/ewEK+uhBxPGYhtXpwhLYwIVIkHmxMwu7R6WGtcyGYlW1jOA9q1lH3a+SYrog9vDWYIES0VeYyiMvkpz4PQVOT4LEhE7hKebtwhd5fzglS1oG2xYmRwQBanheQQKogbww5DATYIBLlcsH1jNBuFBWnyd+fUna7PQwVsWmbHW8SpGzRvJt9i1YPOtJx864UUsdaU/gzTmNM8lcGbr/5jyI6k91SZpO7Sy8MSv6wtYslFtHMTEUPdVgjDRdJXR9sgghaXruoGiYvkDzQ8R+hSh9Wo49HE0RmYA9D03svju1MbCWAdXYXna1jhr1w== Serge nitrokey"

sshKeyPubKadda :: String
sshKeyPubKadda = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAxWjyVnnWbgi0b/iL0WQoy/2NsTJEEtM8foK4fuQrWKnbYnxb+/bxADsWwKkHJoEZ5nyUOZy1ENEwAmb+aYC0yQlKJzWWUhMkSMH1dcycDpA2qXmH2lw0zMVLo09SnpgyMaEVW9rpPEkTlZSAyeoHv7BTdUkWAqiRxAE7axIwRA3QoLD41MSRHGXH/gtTm75TXMZW4JLTPGPtoQHmkZitaZVduf79HKMtqdZgtpEitNGe0g3PPzsezieBBwbMUjOrFw/dQlhrpZTSFJAp1Z0PQ2fJIMcrQpkhU3VOSSXNWpsWooB6Tc9GaQmvUIbkLQhjkFmvgp5qFISKUl+PFhiqtw== medjoubi@hadar4"

sshKeyPubComSixs :: String
sshKeyPubComSixs = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDavuZ/z/6mYh4QrK95ke4OewP3ONDHQgV3PikfnoV1Z0LoKyV0smK/JS2qfvo3RsGkBQS5kOaOioHdDFUOulBWoKcdJM5UPgym2tzxODyGz1SEXSAGPxphyTbrD52QuLuzA7wol7SyhBjDcHWEK5qNgxq3bvighG2Qs4T52CTcNn5ql0H/LzaILHi8XYUSpQptqnytA6uOkdF4uve3U7kbdlXHW+ZjsRVBc++eb/EhrB4IA1LlCE6ASfesE1brnZqPrgunm33xqb4a2hWb72pR1tjIfp7UiA6z+f1U1ZfMUHaLyod8FY/P4u5FvFHJIXgBXshdYPIsZ8H/WRke7wD9 com-sixs@sixs7"

sshKeyPubElkaim :: String
sshKeyPubElkaim = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAyIk4Eu1ct2NvdjFuM95XdE5MK2OXZ/zNytFc7YXyLNZsBAad+sWz2rt79D4zkIhKHwsB3RumzohCwffZM0TewOJUa0nbeKljDozmlm/7Eoc8XQjzk1dneLS64sNqyTjdxzjhEVywJN+8lPxRwTNsYXr8xoldtooc07rNT3cpoqo+K+R4zLrQZAuuOZTk+ZIfQSKZ3PITVBfxlm0v2UK/FsN/WMKPhuAV4HrbQCFAaqqLlLoHypeItcxtOBOlnU63dWtPq1UmWMLtojQQUEE+dOxq7U0CXM3bAGFZimEaqJP8xpMt8wNeVnMH6nbqW3GYwtDgSq2V/5d+Lf8GHwIXQQ== elkaim@hadar4"

sshKeyPubBerenguer :: String
sshKeyPubBerenguer = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAsWmURdeqRvo9Sg3gT3XbZGdD+rx26sKSeHaG/gAnhVHwpRsT2ThHvjPRKe/+wBAJatpVXRc++MyYi6ok+UxD32mh08w8abaUMIoKP6tmQo9YlxE3bikVUpl0NgkbLO23Ggy6cLFrBswTH05EurP6RGkJqexxeDFBeY+tIlHzoyuK44Bd/Rp9mvWRa5wGkUWSaMjbBA1xSUD+JdYHLO4+r/oq6/Hvf3hne/TjGZ9X1TQYIvsdMlSyBuM3UzKGP4ss/aVeaJ9D5g2HxRTaRFBpIL/6m19QgJZ2ME7UL7nPmNtmWdQaxH7BqWSHkaj6I8nQyi4yb0+FsRpzqxf93tpsEw== berenguer@hadar"

sshKeyPubComSixsSrv4 :: String
sshKeyPubComSixsSrv4 = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAvaO897l/NQNGNTMGMnnsG1uR5/gWDzX3oLRb0XIa/1vSgt+KWQRHgM6Ql2jnUZ99UH1f5NaOFOW5B9sGcT3bhKBAfuYfft08tYnsWtXfMXCgzhfmw0mRefRE7iyYpxHmHSInsbemLnKcLsygs7PqV7K0AispGbtPL6lxWaLY6H0L52lgfIyvqxTh9i2OUIZEAQ6tSxPGug0PxrUej8iwdr0LuHnrz9mzQjr+Ch/uRAhV62Ld5NX8yuyxFi71tsoCjT4JbdmBKB0FaXPuG+YFmN0vxi3/Gk0QpkLBk+8VDoQF2CNl3wj5Lb9c6Vr7Vm72gINKNuOKtOJxfKhr+U+SJQ== com-sixs@srv4.sixs.rcl"

sshKeyPubComDiffabsSrv4 :: String
sshKeyPubComDiffabsSrv4 = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAsEXUrwW8ZdeUpMVpi4/Bdp7bfXYQ9OUOPcRZtO1uTFEjWUW+mAfL9U9jtQ0iAirHUfcHSxJk9NeGcZnLVdlJlD6Se3Yqo8xPOyDgeWOX10jBQRgu/21Hi2aWYJiN61pcLBhvPP+koX1pNgvYtBlsGbzP08jcyoB52kwgDYBtCvA+P+yCd8wZFp2EYejPDnwo1lql9Am9jat7TCXhv546m2QFHrI0IL6yUmXz3vMSp5a362TlRY2rswIQrfxNrxqJ/HXCPxthUsLpr1AOX/Zn/+LIL3y7gwzESRLBs27Tb9gdgUm8QgcXDKSMRrFGGNQ/YSEdXROjeUEQrSr4pzCGrw== com-diffabs@srv4.diffabs.rcl"

sshKeyPubReguerSrv4 :: String
sshKeyPubReguerSrv4 = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw++KzD+GHdXFfBFCAW90bTbaymYGTyM8T4+JVWqVrGp92GinS//Sh3sxE4ODtIR4/cog1kV4ZykLwE6mR/2ve5ZSUoasVN1s/XVtYvwP749NQxYdYPd9bpLttfq2ZeqlaKEqTgpkhe8UQgLD1PHGQAU4ozyw+3oQzV9qW40HI2AvPP6kOOe5o4xOe1NKF9WS4rt2xXDkDYkvBrOiZdDXwQDYH7+xQKD63PbSBQ7VdYNGU+PzlnSI3mMNfHu1dx2hmy5BeJAA3yJS/OpO6U/+koUu4dtamoxObk181QQYZ5iSDkfEIC1H3x/S/Cj4yLJK0Ywke3EUNQRqrKMUHu/K7w== reguer@srv4.diffabs.rcl"
