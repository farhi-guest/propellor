module Propellor.Property.Tar (
	create,
) where

import Control.Monad.IO.Class (liftIO)
import Control.Applicative ((<$>))

import Propellor
import qualified Propellor.Property.Apt as Apt

create:: FilePath -> FilePath -> Property NoInfo
create src tarball = prop `requires` installed
  where
    prop = property ("create tarball " ++ tarball) $
      liftIO $ toResult <$> boolSystem "tar" [Param "czf", Param tarball, Param src]

installed :: Property NoInfo
installed = Apt.installed ["tar"]

